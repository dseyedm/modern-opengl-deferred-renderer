// Cook-Torrance BRDF

/* 

precision highp float; //set default precision in glsl es 2.0

uniform vec3 lightDirection;

varying vec3 varNormal;
varying vec3 varEyeDir;

void main()
{
    // set important material values
    float roughnessValue = 0.3; // 0 : smooth, 1: rough
    float F0 = 0.8; // fresnel reflectance at normal incidence
    float k = 0.2; // fraction of diffuse reflection (specular reflection = 1 - k)
    vec3 lightColor = vec3(0.9, 0.1, 0.1);
    
    // interpolating normals will change the length of the normal, so renormalize the normal.
    vec3 normal = normalize(varNormal);
    
    // do the lighting calculation for each fragment.
    float NdotL = max(dot(normal, lightDirection), 0.0);
    
    float specular = 0.0;
    if(NdotL > 0.0)
    {
        vec3 eyeDir = normalize(varEyeDir);

        // calculate intermediary values
        vec3 halfVector = normalize(lightDirection + eyeDir);
        float NdotH = max(dot(normal, halfVector), 0.0); 
        float NdotV = max(dot(normal, eyeDir), 0.0); // note: this could also be NdotL, which is the same value
        float VdotH = max(dot(eyeDir, halfVector), 0.0);
        float mSquared = roughnessValue * roughnessValue;
        
        // geometric attenuation
        float NH2 = 2.0 * NdotH;
        float g1 = (NH2 * NdotV) / VdotH;
        float g2 = (NH2 * NdotL) / VdotH;
        float geoAtt = min(1.0, min(g1, g2));
     
        // roughness (or: microfacet distribution function)
        // beckmann distribution function
        float r1 = 1.0 / ( 4.0 * mSquared * pow(NdotH, 4.0));
        float r2 = (NdotH * NdotH - 1.0) / (mSquared * NdotH * NdotH);
        float roughness = r1 * exp(r2);
        
        // fresnel
        // Schlick approximation
        float fresnel = pow(1.0 - VdotH, 5.0);
        fresnel *= (1.0 - F0);
        fresnel += F0;
        
        specular = (fresnel * geoAtt * roughness) / (NdotV * NdotL * 3.14);
    }
    
    vec3 finalValue = lightColor * NdotL * (k + specular * (1.0 - k);
    gl_FragColor = vec4(finalValue, 1.0);
}

 
*/

#version 330
uniform vec3 camera_pos_ws;
uniform mat4 inverse_projview;
uniform vec3 light_pos_ws;
uniform vec3 light_color;
uniform sampler2D depth_buffer;
uniform sampler2D albedo_buffer;
uniform sampler2D normal_buffer;
uniform sampler2D materials_buffer; // r = roughness, g = fresnel reflectance, b = specular reflection
in vec2 frag_uv_cs;
in vec2 frag_uv_ss;
out vec4 out_final;
// http://content.gpwiki.org/D3DBook:%28Lighting%29_Cook-Torrance
#define BECKMANN_ROUGHNESS 
vec4 cook_torrance(
	in vec3 normal, 
	in vec3 viewer, 
	in vec3 light, 
	in float roughness_value, 
	in float ref_at_norm_incidence, 
	in vec3 cSpecular, 
	in vec3 cAlbedo, 
	in vec3 inLightColor) {
	// Compute any aliases and intermediary values
	vec3 half_vector = normalize(light + viewer);
	float NdotL = clamp(dot(normal, light), 0.0, 1.0);
	
	vec3 final = vec3(0.0);
	if(NdotL > 0.0) {
		float NdotH = clamp(dot(normal, half_vector), 0.0, 1.0);
		float NdotV = clamp(dot(normal, viewer), 0.0, 1.0);
		float VdotH = clamp(dot(viewer, half_vector), 0.0, 1.0);
		float r_sq = roughness_value * roughness_value;

		// Evaluate the geometric term
		float geo_numerator = 2.0f * NdotH;
		float geo_denominator = VdotH;

		float geo_b = (geo_numerator * NdotV) / geo_denominator;
		float geo_c = (geo_numerator * NdotL) / geo_denominator;
		float geo = min(1.0f, min(geo_b, geo_c));

		// Now evaluate the roughness term
		float roughness;

	#ifdef LOOK_UP_ROUGHNESS
	/*
		// texture coordinate is:
		float2 tc = { NdotH, roughness_value };

		// Remap the NdotH value to be 0.0-1.0
		// instead of -1.0..+1.0
		tc.x += 1.0f;
		tc.x /= 2.0f;

		// look up the coefficient from the texture:
		roughness = texRoughness.Sample( sampRoughness, tc );
	*/
	#endif
	#ifdef BECKMANN_ROUGHNESS
		float roughness_a = 1.0f / (4.0f * r_sq * pow(NdotH, 4));
		float roughness_b = NdotH * NdotH - 1.0f;
		float roughness_c = r_sq * NdotH * NdotH;

		roughness = roughness_a * exp(roughness_b / roughness_c);
	#endif
	#ifdef GAUSSIAN_ROUGHNESS
	/*
		// This variable could be exposed as a variable
		// for the application to control:
		float c = 1.0f;
		float alpha = acos( dot( normal, half_vector ) );
		roughness = c * exp( -( alpha / r_sq ) );
	*/
	#endif

		// Next evaluate the Fresnel value
		float fresnel = pow(1.0f - VdotH, 5.0f);
		fresnel *= (1.0f - ref_at_norm_incidence);
		fresnel += ref_at_norm_incidence;

		// Put all the terms together to compute
		// the specular term in the equation
		vec3 Rs_numerator = vec3(fresnel * geo * roughness);
		float Rs_denominator = NdotV * NdotL;
		vec3 Rs = Rs_numerator/ Rs_denominator;

		// Put all the parts together to generate
		// the final colour
		final = max(0.0f, NdotL) * (cSpecular * Rs + cAlbedo);
		final *= inLightColor;
	}
	// Return the result
	return vec4(final, 1.0f);
}
void main() {
	// depth calculations
	// float linear_depth = (2.0 * camera_near) / (camera_far + camera_near - non_linear_depth * (camera_far - camera_near));
	float non_linear_depth = texture(depth_buffer, frag_uv_ss).r;
	if(non_linear_depth >= 0.999) {
		discard;
	}
	float non_linear_depth_cs = non_linear_depth * 2.0 - 1.0;

	// fragment position calculations
	vec4 frag_pos_ss = vec4(frag_uv_cs.xy, non_linear_depth_cs, 1.0);
	vec4 frag_pos_ws_temp = inverse_projview * frag_pos_ss;
	vec3 frag_pos_ws = frag_pos_ws_temp.xyz / frag_pos_ws_temp.w;

	// material values
	vec3 material_albedo = texture(albedo_buffer, frag_uv_ss).rgb;
	vec3 material_properties = texture(materials_buffer, frag_uv_ss).rgb;

	// lighting calculation
	vec3 light_dir_ws = normalize(light_pos_ws - frag_pos_ws);
	vec3 eye_dir_ws = normalize(camera_pos_ws - frag_pos_ws);
	vec3 surface_normal_ws = texture(normal_buffer, frag_uv_ss).xyz * 2.0 - 1.0;
	
	vec3 cook_torrance_value = vec3(cook_torrance(
		surface_normal_ws, 
		eye_dir_ws, 
		light_dir_ws, 
		material_properties.r, 
		material_properties.g, 
		vec3(material_properties.b) * material_albedo, 
		material_albedo, 
		light_color
	));
	out_final = vec4(
		cook_torrance_value.rgb,
		1.0
	);
}
