#version 330
uniform sampler2D blit_texture;
uniform vec2 uv_offset;
uniform vec2 uv_scale;
uniform vec4 color;
in vec2 frag_uv_ss;
void main() {
	gl_FragColor = texture(blit_texture, frag_uv_ss * uv_scale + uv_offset) * color;
}
