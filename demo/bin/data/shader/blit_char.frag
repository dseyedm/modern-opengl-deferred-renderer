#version 330
uniform sampler2D blit_texture;
in vec2 frag_uv_ss;
void main() {
	gl_FragColor = texture(blit_texture, frag_uv_ss);
}
