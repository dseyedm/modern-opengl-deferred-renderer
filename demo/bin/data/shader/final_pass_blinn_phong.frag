#version 330
uniform vec3 camera_pos_ws;
uniform mat4 inverse_projview;
uniform sampler2D depth;
uniform sampler2D diffuse;
uniform sampler2D normal;
in vec2 frag_uv_cs;
in vec2 frag_uv_ss;
void main() {
	// depth calculations
	float non_linear_depth = texture(depth, frag_uv_ss).r;
	//float linear_depth = (2.0 * camera_near) / (camera_far + camera_near - non_linear_depth * (camera_far - camera_near));
	float non_linear_depth_cs = non_linear_depth * 2.0 - 1.0;

	// fragment position calculations
	vec4 frag_pos_ss = vec4(frag_uv_cs.xy, non_linear_depth_cs, 1.0);
	vec4 frag_pos_ws_temp = inverse_projview * frag_pos_ss;
	vec3 frag_pos_ws = frag_pos_ws_temp.xyz / frag_pos_ws_temp.w;

	// normal calculations
	vec3 surface_normal_ws = texture(normal, frag_uv_ss).xyz * 2.0 - 1.0;

	// scene settings
	vec3 ambient_term = vec3(0.005);

	// lighting settings
	vec3 light_color = vec3(1.0, 1.0, 1.0);
	vec3 light_pos_ws = vec3(0.2);

	// material settings
	vec3 material_diffuse = texture(diffuse, frag_uv_ss).rgb;
	vec3 material_specular = vec3(0.1);
	float material_shine = 128.0;

	// lighting calculations
	// ambient
	vec3 ambient_component = ambient_term * material_diffuse;

	// diffuse
	vec3 light_dir_ws = normalize(light_pos_ws - frag_pos_ws);
	float diffuse_term = pow(max(dot(light_dir_ws, surface_normal_ws), 0.0), 2.0);
	vec3 diffuse_component = diffuse_term * material_diffuse * light_color;

	// specular
	vec3 view_dir_ws = normalize(camera_pos_ws - frag_pos_ws); // eye dir
	// phong
	//vec3 reflect_dir_ws = reflect(-light_dir_ws, surface_normal_ws);
	//float specular_term = pow(max(dot(view_dir_ws, reflect_dir_ws), 0.0), material_shine);
	// blinn-phong
	vec3 halfway_dir_ws = normalize(light_dir_ws + view_dir_ws);
	float specular_term = pow(max(dot(halfway_dir_ws, surface_normal_ws), 0.0), material_shine);
	vec3 specular_component = specular_term * material_specular * light_color;

	// final fragment calculation
	gl_FragColor = vec4(
		ambient_component + diffuse_component + specular_component,
		1.0
	);
}
