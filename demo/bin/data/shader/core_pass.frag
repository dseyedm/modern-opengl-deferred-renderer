#version 330
uniform sampler2D material_albedo;
uniform sampler2D material_normal;
uniform vec3 material_color;
uniform float material_roughness;
uniform float material_fresnel_reflection;
uniform float material_specular_reflection;
in vec3 frag_normal_os;
in vec2 frag_uv_os;
in vec3 frag_tangent_ws;
in vec3 frag_bitangent_ws;
out vec4 out_albedo;
out vec4 out_normal;
out vec4 out_materials; // (r = roughness, g = fresnel_reflection, b = specular_reflection)
void main() {
	vec3 normal = normalize(frag_normal_os);
	vec3 tangent = normalize(frag_tangent_ws);
	vec3 bitangent = normalize(frag_bitangent_ws);
	tangent = normalize(tangent - dot(tangent, normal) * normal);	
	vec3 bump_map_normal = texture(material_normal, frag_uv_os).xyz * 2.0 - 1.0;
	mat3 tbn = mat3(tangent, bitangent, normal);
	vec3 combined_normal = tbn * bump_map_normal;
	combined_normal = normalize(combined_normal);

	out_albedo = vec4(material_color.rgb, 1.0) * texture(material_albedo, frag_uv_os);
	out_normal = vec4((combined_normal) * 0.5 + 0.5, 1.0);
	out_materials = vec4(material_roughness, material_fresnel_reflection, material_specular_reflection, 1.0);
}
