// note that the blit_texture is rendered to the top-right of the position value
#version 330
uniform vec2 scale;
uniform vec2 position;
in vec3 quad_cs;
out vec2 frag_uv_ss;
void main() {
	frag_uv_ss = quad_cs.xy * 0.5 + 0.5;
	gl_Position = vec4(vec3(quad_cs.xy * scale + position + scale, 0.0), 1.0);
}
