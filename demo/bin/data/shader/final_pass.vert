#version 330
in vec3 a_quad_cs;
out vec2 a_frag_uv_cs;
out vec2 a_frag_uv_ss;
void main() {
	a_frag_uv_cs = a_quad_cs.xy;
	a_frag_uv_ss = a_quad_cs.xy * 0.5 + 0.5;
	gl_Position = vec4(a_quad_cs, 1.0);
}
