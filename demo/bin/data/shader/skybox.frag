#version 330
uniform samplerCube skybox;
in vec3 frag_uv;
void main () {
	gl_FragColor = textureCube(skybox, frag_uv);
}
