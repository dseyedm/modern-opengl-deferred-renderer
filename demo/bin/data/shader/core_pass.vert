#version 330
uniform mat4 proj_view;
uniform mat4 model;
uniform vec2 model_uv_scale;
uniform mat3 transpose_inverse_model;
in vec3 mesh_vertex_os;
in vec3 mesh_normal_os;
in vec2 mesh_uv_os;
in vec3 mesh_tangent;
in vec3 mesh_bitangent;
out vec2 frag_uv_os;
out vec3 frag_normal_os;
out vec3 frag_tangent_ws;
out vec3 frag_bitangent_ws;
void main() {
	frag_uv_os = model_uv_scale * mesh_uv_os;
	frag_normal_os = transpose_inverse_model * mesh_normal_os;
	frag_tangent_ws = vec3(model * vec4(mesh_tangent, 1.0));
	frag_bitangent_ws = vec3(model * vec4(mesh_bitangent, 1.0));
	gl_Position = proj_view * model * vec4(mesh_vertex_os, 1.0);
}
