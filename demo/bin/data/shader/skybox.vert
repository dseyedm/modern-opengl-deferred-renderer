#version 330
uniform mat4 proj_view;
in vec3 cube_os;
out vec3 frag_uv;
void main() {
	frag_uv = -cube_os;
	gl_Position = vec4(proj_view * vec4(-cube_os, 1.0)).xyww;
}
