// Cook-Torrance BRDF

/* 

precision highp float; //set default precision in glsl es 2.0

uniform vec3 lightDirection;

varying vec3 varNormal;
varying vec3 varEyeDir;

void main()
{
    // set important material values
    float roughnessValue = 0.3; // 0 : smooth, 1: rough
    float F0 = 0.8; // fresnel reflectance at normal incidence
    float k = 0.2; // fraction of diffuse reflection (specular reflection = 1 - k)
    vec3 lightColor = vec3(0.9, 0.1, 0.1);
    
    // interpolating normals will change the length of the normal, so renormalize the normal.
    vec3 normal = normalize(varNormal);
    
    // do the lighting calculation for each fragment.
    float NdotL = max(dot(normal, lightDirection), 0.0);
    
    float specular = 0.0;
    if(NdotL > 0.0)
    {
        vec3 eyeDir = normalize(varEyeDir);

        // calculate intermediary values
        vec3 halfVector = normalize(lightDirection + eyeDir);
        float NdotH = max(dot(normal, halfVector), 0.0); 
        float NdotV = max(dot(normal, eyeDir), 0.0); // note: this could also be NdotL, which is the same value
        float VdotH = max(dot(eyeDir, halfVector), 0.0);
        float mSquared = roughnessValue * roughnessValue;
        
        // geometric attenuation
        float NH2 = 2.0 * NdotH;
        float g1 = (NH2 * NdotV) / VdotH;
        float g2 = (NH2 * NdotL) / VdotH;
        float geoAtt = min(1.0, min(g1, g2));
     
        // roughness (or: microfacet distribution function)
        // beckmann distribution function
        float r1 = 1.0 / ( 4.0 * mSquared * pow(NdotH, 4.0));
        float r2 = (NdotH * NdotH - 1.0) / (mSquared * NdotH * NdotH);
        float roughness = r1 * exp(r2);
        
        // fresnel
        // Schlick approximation
        float fresnel = pow(1.0 - VdotH, 5.0);
        fresnel *= (1.0 - F0);
        fresnel += F0;
        
        specular = (fresnel * geoAtt * roughness) / (NdotV * NdotL * 3.14);
    }
    
    vec3 finalValue = lightColor * NdotL * (k + specular * (1.0 - k);
    gl_FragColor = vec4(finalValue, 1.0);
}

 
*/

#version 330
uniform vec3 u_camera_pos_ws;
uniform mat4 u_inverse_projview;
uniform vec3 u_light_pos_ws;
uniform vec3 u_light_color;
uniform sampler2D u_depth_buffer;
uniform sampler2D u_albedo_buffer;
uniform sampler2D u_normal_buffer;
uniform sampler2D u_materials_buffer; // r = roughness, g = fresnel reflectance, b = specular reflection
in vec2 a_frag_uv_cs;
in vec2 a_frag_uv_ss;
out vec4 out_final;
vec3 cookTorrance(
	in float material_roughness,
	in float material_fresnel,
	in float material_specular,
	in vec3 light_color,
	in vec3 light_direction,
	in vec3 eye_direction,
	in vec3 surface_normal
) {
	float k = 1.0 - material_specular; // fraction of diffuse reflection (specular reflection = 1 - k)
	float n_dot_l = max(dot(surface_normal, light_direction), 0.0);
	float specular = 0.0;
	if(n_dot_l > 0.0) {
		// calculate intermediary values
		vec3 half_vector = normalize(light_direction + eye_direction);
		float n_dot_h = max(dot(surface_normal, half_vector), 0.0); 
		float n_dot_v = max(dot(surface_normal, eye_direction), 0.0); // note: this could also be n_dot_l, which is the same value
		float v_dot_h = max(dot(eye_direction, half_vector), 0.0);
		float roughness_sq = material_roughness * material_roughness;

		// geometric attenuation
		float nh2 = 2.0 * n_dot_h;
		float g1 = (nh2 * n_dot_v) / v_dot_h;
		float g2 = (nh2 * n_dot_l) / v_dot_h;
		float geometric_attenuation = min(1.0, min(g1, g2));

		// roughness (or: microfacet distribution function)
		// beckmann distribution function
		float r1 = 1.0 / (4.0 * roughness_sq * pow(n_dot_h, 4.0));
		float r2 = (n_dot_h * n_dot_h - 1.0) / (roughness_sq * n_dot_h * n_dot_h);
		float roughness = r1 * exp(r2);

		// fresnel
		// Schlick approximation
		float fresnel = pow(1.0 - v_dot_h, 5.0);
		fresnel *= (1.0 - material_fresnel);
		fresnel += material_fresnel;

		specular = (fresnel * geometric_attenuation * roughness) / (n_dot_v * n_dot_l * 3.14159);
	}
	return light_color * n_dot_l * (k + specular * (1.0 - k));
}
vec3 getFragPosWS(vec2 frag_uv_cs, in float non_linear_depth_cs, in mat4 inverse_projview) {
	vec4 frag_pos_ss = vec4(frag_uv_cs, non_linear_depth_cs, 1.0);
	vec4 temp = inverse_projview * frag_pos_ss;
	return temp.xyz / temp.w;
}
float getLinearDepth(in float non_linear_depth, in float camera_near, in float camera_far) {
	return (2.0 * camera_near) / (camera_far + camera_near - non_linear_depth * (camera_far - camera_near));
}
void main() {
	// depth constants
	float non_linear_depth = texture(u_depth_buffer, a_frag_uv_ss).r;
	if(non_linear_depth >= 0.999) discard;
	float non_linear_depth_cs = non_linear_depth * 2.0 - 1.0;

	// fragment position constant
	vec3 frag_pos_ws = getFragPosWS(a_frag_uv_cs, non_linear_depth_cs, u_inverse_projview);

	// material constants
	vec3 material_albedo = texture(u_albedo_buffer, a_frag_uv_ss).rgb;
	float material_roughness = texture(u_materials_buffer, a_frag_uv_ss).r;
	float material_fresnel = texture(u_materials_buffer, a_frag_uv_ss).g;
	float material_specular = texture(u_materials_buffer, a_frag_uv_ss).b;

	// lighting constants
	vec3 light_dir_ws = normalize(u_light_pos_ws - frag_pos_ws);
	vec3 eye_dir_ws = normalize(u_camera_pos_ws - frag_pos_ws);
	vec3 surface_normal_ws = texture(u_normal_buffer, a_frag_uv_ss).xyz * 2.0 - 1.0;
	
	// cook torrance brdf
	vec3 cook_torrance = cookTorrance(
		material_roughness,
		material_fresnel,
		material_specular,
		u_light_color * material_albedo,
		light_dir_ws,
		eye_dir_ws,
		surface_normal_ws
	);
	
	// output
	out_final = vec4(
		cook_torrance.rgb,
		1.0
	);
}
