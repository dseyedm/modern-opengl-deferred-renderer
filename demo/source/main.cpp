/*
lyft demo
author:
	soraj seyed-mahmoud
scene:
	statuettes (marble, metal, wood, plastic, etc, http://josecorreia.com/texturas.php)
	floor
	skybox
	point lights
	spot lights
	directional light
	shadows
gfx:
	ssao
	environment mapping
	multiple lights
	shadow mapping
	antialiasing
	gamma adjust
	tone mapping
	bloom

gbuffer:
depth	depth
rgb8	albedo
rgb8	normals
rgba8	materials (specular level, specular exponent, metal level, roughness)
r8		ssao

post process motion blur
post process god rays
screen space reflections
baked shadows
*/

// todo: angular velocity
// todo: fix teapot uvs

// todo: add a #define system:
// shader core_uber[diffuse][normal][material];
// todo: add support for 360* photo to cubemap loading
// todo: cli, font rendering, box rendering

#include "demo/programs.hpp"
#include "demo/materials.hpp"
#include "demo/textures.hpp"
#include "demo/meshes.hpp"
#include "demo/renderer.hpp"

#include "base/camera.hpp"
#include "base/model.hpp"

#include "base/error.hpp"

#include <iostream>
using namespace std;

struct PointLight {
	vec3 pos { 5.0 }, color { 1.0 };
};

int context(GLFWwindow* window) {
	int screen_width, screen_height;
	glfwGetWindowSize(window, &screen_width, &screen_height);

	// load resources
	createPrograms();
	if(!loadPrograms()) {
		printFailedToLoad("programs");
		deletePrograms();
		return 1;
	}
	createMaterials();
	if(!loadMaterials()) {
		printFailedToLoad("materials");
		deletePrograms();
		deleteMaterials();
		return 1;
	}
	createTextures();
	if(!loadTextures()) {
		printFailedToLoad("textures");
		deletePrograms();
		deleteMaterials();
		deleteTextures();
		return 1;
	}
	createMeshes();
	if(!loadMeshes()) {
		printFailedToLoad("meshes");
		deletePrograms();
		deleteMaterials();
		deleteTextures();
		deleteMeshes();
		return 1;
	}
	createRenderer();
	if(!loadRenderer(screen_width, screen_height)) {
		printFailedToLoad("renderer");
		deletePrograms();
		deleteMaterials();
		deleteTextures();
		deleteMeshes();
		deleteRenderer();
		return 1;
	}

	// attach mesh resources to appropriate program attributes
	attachMeshAttributes(msh::render_quad, final_pass::mesh_vertex, Attribute(), Attribute(), Attribute(), Attribute());
	attachMeshAttributes(msh::render_quad, blit_texture::mesh_vertex, Attribute(), Attribute(), Attribute(), Attribute());
	attachMeshAttributes(msh::cube, skybox::mesh_vertex, Attribute(), Attribute(), Attribute(), Attribute());
	attachMeshAttributes(msh::teapot, core_pass::mesh_vertex, core_pass::mesh_normal, core_pass::mesh_uv, core_pass::mesh_tangent, core_pass::mesh_bitangent);
	attachMeshAttributes(msh::sphere, core_pass::mesh_vertex, core_pass::mesh_normal, core_pass::mesh_uv, core_pass::mesh_tangent, core_pass::mesh_bitangent);
	attachMeshAttributes(msh::plane, core_pass::mesh_vertex, core_pass::mesh_normal, core_pass::mesh_uv, core_pass::mesh_tangent, core_pass::mesh_bitangent);

	// models
	const uint models_x_sz = 5, models_y_sz = 1;
	Model models[models_y_sz][models_x_sz];
	for(uint y = 0; y < models_y_sz; ++y) {
		for(uint x = 0; x < models_x_sz; ++x) {
			models[y][x].msh = &msh::sphere;
			models[y][x].mat = mat::wood;
			models[y][x].mat.color = vec3(1.0);
			models[y][x].mat.roughness = (x + 1.0) / real32(models_x_sz);
			models[y][x].mat.specular_reflection = models_x_sz - (x + 1.0) / real32(models_x_sz);
			models[y][x].mat.fresnel_reflection = models_x_sz - (x + 1.0) / real32(models_x_sz);
			models[y][x].pos = vec3(x / 3.0, 0.0, y / 3.0);
			models[y][x].scl = vec3(0.2);
			models[y][x].uv_scl = vec2(0.0);
		}
	}

	// lighting
	PointLight point_light;

	// camera
	Camera camera;
	camera.far = 1000.0;
	camera.near = 0.01;
	camera.pos = vec3(1.5);
	lookAt(camera.orient, camera.pos, vec3((models_x_sz - 1.0) / 6.0, 0.0, (models_y_sz - 1.0) / 6.0));

	// static opengl state
	gl(Enable(GL_CULL_FACE));
	gl(CullFace(GL_BACK));
	gl(Disable(GL_STENCIL_TEST));
	gl(Disable(GL_BLEND));
	gl(ClearColor(0.0, 0.0, 0.0, 1.0));

	// main loop
	while(!glfwWindowShouldClose(window) && !glfwGetKey(window, GLFW_KEY_ESCAPE)) {
#ifdef NDEBUG
		// check for errors
		GLuint error = glGetError();
		while(error != GL_NO_ERROR) {
			std::cerr << "glGetError returned " << error << std::endl;
			error = glGetError();
			if(error == GL_NO_ERROR) {
				return 1;
			}
		}
#endif
		// update window
		glfwPollEvents();
		real32 curt = glfwGetTime(), dts;
		{
			static real32 last = 0.0;
			dts = curt - last;
			last = curt;
		}
		int last_screen_width = screen_width, last_screen_height = screen_height;
		glfwGetWindowSize(window, &screen_width, &screen_height);
		if(last_screen_width != screen_width || last_screen_height != screen_height) {
			resizeRenderer(screen_width, screen_height);
			camera.aspect = zero(screen_height) ? 1.0 : real32(screen_width) / screen_height;
		}

		bool pressing_f_key[12];
		static bool pressed_f_key[12];
		for(uint i = 0; i < 12; ++i) {
			pressing_f_key[i] = glfwGetKey(window, GLFW_KEY_F1 + i);
		}
		if(pressing_f_key[4] && !pressed_f_key[4]) {
			if(loadPrograms()) cout << "reloaded programs successfully" << endl;
		}
		if(pressing_f_key[5] && !pressed_f_key[5]) {
			if(loadMaterials()) cout << "reloaded materials successfully" << endl;
		}
		if(pressing_f_key[6] && !pressed_f_key[6]) {
			if(loadTextures()) cout << "reloaded textures successfully" << endl;
		}
		if(pressing_f_key[7] && !pressed_f_key[7]) {
			if(loadMeshes()) cout << "reloaded meshes successfully" << endl;
		}
		for(uint i = 0; i < 12; ++i) {
			pressed_f_key[i] = pressing_f_key[i];
		}

		// update camera
		if(glfwGetKey(window, GLFW_KEY_LEFT_SHIFT)) dts /= 4.0;
		transformCamera(window, camera, 0.003, 0.002, dts);

		static bool stop = false;
		bool pressing_tab = glfwGetKey(window, GLFW_KEY_TAB);
		static bool pressed_tab = pressing_tab;
		if(pressing_tab && !pressed_tab) stop = !stop;
		pressed_tab = pressing_tab;
		if(stop) dts = 0.0;

		// update common shader values
		mat4 view = cameraView(camera);
		mat4 proj = cameraProj(camera);
		mat4 proj_view = proj * view;
		mat4 inverse_projview = glm::inverse(proj_view);

		if(pressing_f_key[0]) point_light.pos = camera.pos;

		// render to gbuffer
		bindFbo(rdr::fbo);
		{
			gl(Enable(GL_DEPTH_TEST));
			gl(Disable(GL_FRAMEBUFFER_SRGB));

			gl(Clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
			gl(Viewport(0, 0, screen_width, screen_height));

			bindProgram(core_pass::program);
			setUniform(core_pass::material_albedo, 0);
			setUniform(core_pass::material_normal, 1);
			setUniform(core_pass::proj_view, proj_view);
			for(uint y = 0; y < models_y_sz; ++y) {
				for(uint x = 0; x < models_x_sz; ++x) {
					mat4 model_mat = modelMat4(models[y][x]);
					setUniform(core_pass::model, model_mat);
					setUniform(core_pass::model_uv_scale, models[y][x].uv_scl);
					setUniform(core_pass::transpose_inverse_model, mat3(transpose(inverse(model_mat))));
					setUniform(core_pass::material_color, models[y][x].mat.color);
					setUniform(core_pass::material_roughness, models[y][x].mat.roughness);
					setUniform(core_pass::material_fresnel_reflection, models[y][x].mat.fresnel_reflection);
					setUniform(core_pass::material_specular_reflection, models[y][x].mat.specular_reflection);
					bindTexture2D(models[y][x].mat.albedo, 0);
					bindTexture2D(models[y][x].mat.normal, 1);
					renderMeshElements(*models[y][x].msh);
					unbindTexture2D(1);
					unbindTexture2D(0);
				}
			}
			unbindProgram();
		}
		// render to screen
		unbindFbo();
		{
			gl(Disable(GL_DEPTH_TEST));
			gl(Enable(GL_FRAMEBUFFER_SRGB));

			// render scene
			gl(Clear(GL_COLOR_BUFFER_BIT));
			gl(Viewport(0, 0, screen_width, screen_height));

			// render skybox
			bindProgram(skybox::program);
			setUniform(skybox::proj_view, proj * cameraView(camera.orient, vec3(0.0)));
			setUniform(skybox::skybox, 0);
			bindTextureCube(tex::snow_cubemap, 0);
			renderMeshElements(msh::cube);
			unbindTextureCube();
			unbindProgram();

			// render lights
			bindProgram(final_pass::program);
			setUniform(final_pass::camera_pos_ws, camera.pos);
			setUniform(final_pass::inverse_projview, inverse_projview);
			setUniform(final_pass::point_light_color, point_light.color);
			setUniform(final_pass::point_light_pos, point_light.pos);
			setUniform(final_pass::depth, 0);
			setUniform(final_pass::albedo, 1);
			setUniform(final_pass::normal, 2);
			setUniform(final_pass::materials, 3);
			bindTexture2D(rdr::depth, 0);
			bindTexture2D(rdr::albedo, 1);
			bindTexture2D(rdr::normals, 2);
			bindTexture2D(rdr::materials, 3);
			renderMeshVertices(msh::render_quad, GL_TRIANGLE_STRIP);
			unbindTexture2D(3);
			unbindTexture2D(2);
			unbindTexture2D(1);
			unbindTexture2D(0);
			unbindProgram();

        	// display gbuffers
			bindProgram(blit_texture::program);
			setUniform(blit_texture::color, vec4(1.0));
			setUniform(blit_texture::uv_scale, vec2(1.0));
			setUniform(blit_texture::uv_offset, vec2(0.0));
			bindTexture2D(rdr::depth);
			setUniform(blit_texture::position, vec2(-0.8, -0.8));
			setUniform(blit_texture::scale, vec2(0.2));
			setUniform(blit_texture::blit_texture, 0);
			renderMeshVertices(msh::render_quad, GL_TRIANGLE_STRIP);
			unbindTexture2D();
			bindTexture2D(rdr::albedo);
			setUniform(blit_texture::position, vec2(-0.4, -0.8));
			setUniform(blit_texture::scale, vec2(0.2));
			setUniform(blit_texture::blit_texture, 0);
			renderMeshVertices(msh::render_quad, GL_TRIANGLE_STRIP);
			unbindTexture2D();
			bindTexture2D(rdr::normals);
			setUniform(blit_texture::position, vec2(-0.0, -0.8));
			setUniform(blit_texture::scale, vec2(0.2));
			setUniform(blit_texture::blit_texture, 0);
			renderMeshVertices(msh::render_quad, GL_TRIANGLE_STRIP);
			unbindTexture2D();
			bindTexture2D(rdr::materials);
			setUniform(blit_texture::position, vec2(0.4, -0.8));
			setUniform(blit_texture::scale, vec2(0.2));
			setUniform(blit_texture::blit_texture, 0);
			renderMeshVertices(msh::render_quad, GL_TRIANGLE_STRIP);
			unbindTexture2D();

			// render text
			gl(Enable(GL_BLEND));
			gl(BlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));

			static const char* table = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789$-+*/%\"'#@_(),.;:?!\\|{}<>[]^~&` ";
			static const int table_len = strlen(table), table_size_x = 658, table_size_y = 11, char_size_x = 7, char_size_y = 11;
			static const char* str = "Hello, World!";
			static const int str_len = strlen(str);

			bindTexture2D(tex::font_8bitoperator);
			setUniform(blit_texture::blit_texture, 0);
			setUniform(blit_texture::scale, vec2(real32(char_size_x) / screen_width, real32(char_size_y) / screen_height) * vec2(4.0));
			setUniform(blit_texture::uv_scale, vec2(7.0 / real32(table_size_x), 1.0));
			// yes, we are doing an O(n) search, but it's okay... for now.
			for(int i = 0; i < str_len; ++i) {
				setUniform(blit_texture::color, vec4(vec3(std::abs(std::cos(i / real32(str_len))), std::abs(std::sin(i / real32(str_len))), 1.0 - std::abs(std::cos(i / real32(str_len)))) * vec3(std::abs(std::cos(curt)), std::abs(std::sin(curt)), 1.0 - std::abs(std::cos(curt))), 1.0));
				setUniform(blit_texture::position, vec2((i - str_len / 2.0) * char_size_x, 0.0) / vec2(screen_width, screen_height) * vec2(2.5) * vec2(4.0));
				for(int j = 0; j < table_len; ++j) {
					if(str[i] != table[j]) continue;
					setUniform(blit_texture::uv_offset, vec2(j / (real32(table_size_x) / 7.0), 0.0));
					renderMeshVertices(msh::render_quad, GL_TRIANGLE_STRIP);
					break;
				}
			}
			unbindTexture2D();

			gl(Disable(GL_BLEND));

			unbindProgram();
        }
        // draw
		glfwSwapBuffers(window);
	}
	deletePrograms();
	deleteMaterials();
	deleteTextures();
	deleteMeshes();
	deleteRenderer();
	return 0;
}
int main() {
	if(!glfwInit()) {
		std::cerr << "failed to initialize glfw" << std::endl;
		return 1;
	}
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	GLFWwindow* window = glfwCreateWindow(800, 600, "Lyft 3D", nullptr, nullptr);
	if(!window) {
		std::cerr << "failed to open glfw window" << std::endl;
		glfwTerminate();
		return 1;
	}
	glfwMakeContextCurrent(window);
	if(gl3wInit() != 0 || !gl3wIsSupported(3, 3)) {
		std::cerr << "failed to initialize opengl 3.3 context" << std::endl;
		glfwTerminate();
		return 1;
	}
	int error = context(window);
	if(error) {
		std::cerr << "type anything and press enter to exit: ";
		string pause;
		std::cin.clear();
		std::cin >> pause;
	}
	glfwDestroyWindow(window);
	glfwTerminate();
	return error;
}
