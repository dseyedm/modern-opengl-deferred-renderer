#include "demo/meshes.hpp"
#include "base/error.hpp"

namespace msh {
Mesh render_quad, cube, teapot, plane, sphere;
}
using namespace msh;

void createMeshes() {
	createMesh(render_quad);
	createMesh(cube);
	createMesh(teapot);
	createMesh(plane);
	createMesh(sphere);
}
bool loadMeshes() {
	render_quad.vertices = {
		{ -1.0, -1.0, 0.0 },
		{  1.0, -1.0, 0.0 },
		{ -1.0,  1.0, 0.0 },
		{  1.0,  1.0, 0.0 },
	};
	uploadMesh(render_quad);

	if(!loadObjMesh(cube, "data/mesh/cube.obj")) {
		printFailedToLoad("data/mesh/cube.obj");
		return false;
	}
	generateMeshTangents(cube);
	uploadMesh(cube);

	if(!loadObjMesh(teapot, "data/mesh/teapot.obj")) {
		printFailedToLoad("data/mesh/teapot.obj");
		return false;
	}
	generateMeshTangents(teapot);
	uploadMesh(teapot);

	if(!loadObjMesh(plane, "data/mesh/plane.obj")) {
		printFailedToLoad("data/mesh/plane.obj");
		return false;
	}
	generateMeshTangents(plane);
	uploadMesh(plane);

	if(!loadObjMesh(sphere, "data/mesh/sphere.obj")) {
		printFailedToLoad("data/mesh/sphere.obj");
		return false;
	}
	generateMeshTangents(sphere);
	uploadMesh(sphere);

	return true;
}
void deleteMeshes() {
	deleteMesh(render_quad);
	deleteMesh(cube);
	deleteMesh(teapot);
	deleteMesh(plane);
	deleteMesh(sphere);
}
