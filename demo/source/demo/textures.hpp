#ifndef DEMO_TEXTURES_HPP
#define DEMO_TEXTURES_HPP

#include "base/image.hpp"

namespace tex {
extern TextureCube snow_cubemap;
extern Texture2D font_8bitoperator;
}

void createTextures();
bool loadTextures();
void deleteTextures();

#endif
