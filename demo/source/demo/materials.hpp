#ifndef DEMO_MATERIALS_HPP
#define DEMO_MATERIALS_HPP

#include "base/material.hpp"

namespace mat {
extern Material wood;
}

void createMaterials();
bool loadMaterials();
void deleteMaterials();

#endif
