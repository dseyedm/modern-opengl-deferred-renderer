#ifndef DEMO_RENDERER_HPP
#define DEMO_RENDERER_HPP

#include <lyft/lyft.hpp>

namespace rdr {
extern Fbo fbo, default_fbo;
extern Texture2D depth, albedo, normals, materials;
}

void createRenderer();
bool loadRenderer(uint render_width, uint render_height);
void resizeRenderer(uint render_width, uint render_height);
void deleteRenderer();

#endif
