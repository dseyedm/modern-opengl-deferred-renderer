#include "demo/materials.hpp"
#include "base/error.hpp"

namespace mat {
Material wood;
}
using namespace mat;

void createMaterials() {
	createMaterial(wood);
}
bool loadMaterials() {
	Image wood_albedo, wood_normal;
	if(!loadImage(wood_albedo, STBI_rgb_alpha, "data/material/wood_albedo.bmp", false)) {
		printFailedToLoad("data/material/wood_albedo.bmp");
		return false;
	}
	if(!loadImage(wood_normal, STBI_rgb, "data/material/wood_normal.bmp", false)) {
		printFailedToLoad("data/material/wood_normal.bmp");
		deleteImage(wood_albedo);
		return false;
	}

	bindTexture2D(wood.albedo);
	setFilterTexture2D(GL_LINEAR);
	setWrapTexture2D(GL_REPEAT);
	uploadImageTexture2D(wood_albedo);
	unbindTexture2D();
	bindTexture2D(wood.normal);
	setFilterTexture2D(GL_LINEAR);
	setWrapTexture2D(GL_REPEAT);
	uploadImageTexture2D(wood_normal);
	unbindTexture2D();

	deleteImage(wood_albedo);
	deleteImage(wood_normal);

	return true;
}
void deleteMaterials() {
	deleteMaterial(wood);
}
