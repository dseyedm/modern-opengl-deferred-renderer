#ifndef DEMO_PROGRAMS_HPP
#define DEMO_PROGRAMS_HPP

#include "base/program.hpp"

// absolutely terrible.
// must fix.
// awful. disgusting.

namespace core_pass {
extern Program program;
extern Attribute
	mesh_vertex,
	mesh_normal,
	mesh_uv,
	mesh_tangent,
	mesh_bitangent;
extern Uniform
	proj_view,
	model,
	model_uv_scale,
	transpose_inverse_model,
	material_albedo,
	material_normal,
	material_color,
	material_roughness,
	material_fresnel_reflection,
	material_specular_reflection;
}
namespace final_pass {
extern Program program;
extern Attribute
	mesh_vertex;
extern Uniform
	camera_pos_ws,
	inverse_projview,
	ambient_term,
	point_light_color,
	point_light_pos,
	depth,
	albedo,
	normal,
	materials;
}
namespace blit_texture {
extern Program program;
extern Attribute
	mesh_vertex;
extern Uniform
	blit_texture,
	position,
	scale,
	uv_scale,
	uv_offset,
	color;
}
namespace skybox {
extern Program program;
extern Attribute
	mesh_vertex;
extern Uniform
	proj_view,
	skybox;
}

void createPrograms();
bool loadPrograms();
void deletePrograms();

#endif
