#ifndef DEMO_MESHES_HPP
#define DEMO_MESHES_HPP

#include "base/mesh.hpp"

namespace msh {
extern Mesh render_quad, cube, teapot, plane, sphere;
}

void createMeshes();
bool loadMeshes();
void deleteMeshes();

#endif
