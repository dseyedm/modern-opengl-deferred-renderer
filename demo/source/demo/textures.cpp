#include "demo/textures.hpp"
#include "base/error.hpp"

namespace tex {
TextureCube snow_cubemap;
Texture2D font_8bitoperator;
}
using namespace tex;

void createTextures() {
	createTextureCube(snow_cubemap);
	createTexture2D(font_8bitoperator);
}
bool loadTextures() {
	ImageCube snow_image_cubemap;
	if(!loadImageCube(snow_image_cubemap, STBI_rgb,
			"data/skybox/snow_px.jpg", "data/skybox/snow_nx.jpg",
			"data/skybox/snow_py.jpg", "data/skybox/snow_ny.jpg",
			"data/skybox/snow_pz.jpg", "data/skybox/snow_nz.jpg", false)) {
		printFailedToLoad("data/skybox/snow_xx.jpg");
		deleteImageCube(snow_image_cubemap);
		return false;
	}

	Image font_8bitoperator_image;
	if(!loadImage(font_8bitoperator_image, STBI_rgb_alpha, "data/font/8bitoperator.png")) {
		printFailedToLoad("data/font/8bitoperator.png");
		deleteImage(font_8bitoperator_image);
		return false;
	}

	bindTextureCube(snow_cubemap);
	setFilterTextureCube(GL_LINEAR);
	setWrapTextureCube(GL_CLAMP_TO_EDGE);
	uploadImageTextureCube(snow_image_cubemap);
	unbindTextureCube();

	bindTexture2D(font_8bitoperator);
	setFilterTexture2D(GL_NEAREST);
	setWrapTexture2D(GL_CLAMP_TO_EDGE);
	uploadImageTexture2D(font_8bitoperator_image);
	unbindTexture2D();

	deleteImageCube(snow_image_cubemap);
	deleteImage(font_8bitoperator_image);

	return true;
}
void deleteTextures() {
	deleteTextureCube(snow_cubemap);
	deleteTexture2D(font_8bitoperator);
}
