#include "demo/renderer.hpp"

namespace rdr {
Fbo fbo, default_fbo;
Texture2D depth, albedo, normals, materials;
}

using namespace rdr;

void createRenderer() {
	default_fbo.handle = 0;

	createTexture2D(depth);
	createTexture2D(albedo);
	createTexture2D(normals);
	createTexture2D(materials);
	createFbo(fbo);
}
bool loadRenderer(uint render_width, uint render_height) {
	bindTexture2D(depth);
	setFilterTexture2D(GL_NEAREST);
	setWrapTexture2D(GL_CLAMP_TO_EDGE);
	uploadTexture2D(nullptr, render_width, render_height, GL_UNSIGNED_BYTE, GL_DEPTH_COMPONENT, GL_DEPTH_COMPONENT32);
	unbindTexture2D();
	bindTexture2D(albedo);
	setFilterTexture2D(GL_NEAREST);
	setWrapTexture2D(GL_CLAMP_TO_EDGE);
	uploadTexture2D(nullptr, render_width, render_height, GL_UNSIGNED_BYTE, GL_RGB, GL_RGB8);
	unbindTexture2D();
	bindTexture2D(normals);
	setFilterTexture2D(GL_NEAREST);
	setWrapTexture2D(GL_CLAMP_TO_EDGE);
	uploadTexture2D(nullptr, render_width, render_height, GL_UNSIGNED_BYTE, GL_RGB, GL_RGB8);
	unbindTexture2D();
	bindTexture2D(materials);
	setFilterTexture2D(GL_NEAREST);
	setWrapTexture2D(GL_CLAMP_TO_EDGE);
	uploadTexture2D(nullptr, render_width, render_height, GL_UNSIGNED_BYTE, GL_RGB, GL_RGB8);
	unbindTexture2D();

	bindFbo(fbo);
	GLenum fbo_draw_buffers[3] { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2 };
	setDrawBuffersFbo(fbo_draw_buffers, 3);
	attachTexture2DFbo(GL_DEPTH_ATTACHMENT, depth);
	attachTexture2DFbo(GL_COLOR_ATTACHMENT0, albedo);
	attachTexture2DFbo(GL_COLOR_ATTACHMENT1, normals);
	attachTexture2DFbo(GL_COLOR_ATTACHMENT2, materials);
	lftAssert(getStatusFbo() == GL_FRAMEBUFFER_COMPLETE);
	unbindFbo();

	return true;
}
void resizeRenderer(uint render_width, uint render_height) {
	bindTexture2D(depth);
	uploadTexture2D(nullptr, render_width, render_height, GL_UNSIGNED_BYTE, GL_DEPTH_COMPONENT, GL_DEPTH_COMPONENT32);
	unbindTexture2D();
	bindTexture2D(albedo);
	uploadTexture2D(nullptr, render_width, render_height, GL_UNSIGNED_BYTE, GL_RGB, GL_RGB8);
	unbindTexture2D();
	bindTexture2D(normals);
	uploadTexture2D(nullptr, render_width, render_height, GL_UNSIGNED_BYTE, GL_RGB, GL_RGB8);
	unbindTexture2D();
	bindTexture2D(materials);
	uploadTexture2D(nullptr, render_width, render_height, GL_UNSIGNED_BYTE, GL_RGB, GL_RGB8);
	unbindTexture2D();
}
void deleteRenderer() {
	deleteTexture2D(depth);
	deleteTexture2D(albedo);
	deleteTexture2D(normals);
	deleteTexture2D(materials);
	deleteFbo(fbo);
}
