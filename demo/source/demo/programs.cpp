#include "demo/programs.hpp"
#include "base/error.hpp"

#include <iostream>
using namespace std;

namespace core_pass {
Program program;
Attribute
	mesh_vertex,
	mesh_normal,
	mesh_uv,
	mesh_tangent,
	mesh_bitangent;
Uniform
	proj_view,
	model,
	model_uv_scale,
	transpose_inverse_model,
	material_albedo,
	material_normal,
	material_color,
	material_roughness,
	material_fresnel_reflection,
	material_specular_reflection;
}
namespace final_pass {
Program program;
Attribute
	mesh_vertex;
Uniform
	camera_pos_ws,
	inverse_projview,
	point_light_color,
	point_light_pos,
	depth,
	albedo,
	normal,
	materials;
}
namespace blit_texture {
Program program;
Attribute
	mesh_vertex;
Uniform
	blit_texture,
	position,
	scale,
	uv_scale,
	uv_offset,
	color;
}
namespace skybox {
Program program;
Attribute
	mesh_vertex;
Uniform
	proj_view,
	skybox;
}

void createPrograms() {
	createProgram(core_pass::program);
	createProgram(final_pass::program);
	createProgram(blit_texture::program);
	createProgram(skybox::program);
}
bool loadPrograms() {
	auto get_attribute = [](Attribute& a, const char* id, Program p) {
		a = getAttribute(p, id);
		if(a.handle == GLuint(-1)) {
			printFailedToFind(id);
			return false;
		}
		return true;
	};
	auto get_uniform = [](Uniform& u, const char* id, Program p) {
		u = getUniform(p, id);
		if(u.handle == GLuint(-1)) {
			printFailedToFind(id);
			return false;
		}
		return true;
	};

	string error;
	if(!loadProgram(core_pass::program, "data/shader/core_pass.vert", "data/shader/core_pass.frag", error)) {
		cerr << error << endl;
		return false;
	}
	if(!get_attribute(core_pass::mesh_vertex, "mesh_vertex_os", core_pass::program)) return false;
	if(!get_attribute(core_pass::mesh_normal, "mesh_normal_os", core_pass::program)) return false;
	if(!get_attribute(core_pass::mesh_uv, "mesh_uv_os", core_pass::program)) return false;
	if(!get_attribute(core_pass::mesh_tangent, "mesh_tangent", core_pass::program)) return false;
	if(!get_attribute(core_pass::mesh_bitangent, "mesh_bitangent", core_pass::program)) return false;
	if(!get_uniform(core_pass::proj_view, "proj_view", core_pass::program)) return false;
	if(!get_uniform(core_pass::model, "model", core_pass::program)) return false;
	if(!get_uniform(core_pass::model_uv_scale, "model_uv_scale", core_pass::program)) return false;
	if(!get_uniform(core_pass::transpose_inverse_model, "transpose_inverse_model", core_pass::program)) return false;
	if(!get_uniform(core_pass::material_albedo, "material_albedo", core_pass::program)) return false;
	if(!get_uniform(core_pass::material_normal, "material_normal", core_pass::program)) return false;
	if(!get_uniform(core_pass::material_color, "material_color", core_pass::program)) return false;
	if(!get_uniform(core_pass::material_roughness, "material_roughness", core_pass::program)) return false;
	if(!get_uniform(core_pass::material_fresnel_reflection, "material_fresnel_reflection", core_pass::program)) return false;
	if(!get_uniform(core_pass::material_specular_reflection, "material_specular_reflection", core_pass::program)) return false;

	if(!loadProgram(final_pass::program, "data/shader/final_pass.vert", "data/shader/final_pass.frag", error))  {
		cerr << error << endl;
		return false;
	}
	if(!get_attribute(final_pass::mesh_vertex, "a_quad_cs", final_pass::program)) return false;
	if(!get_uniform(final_pass::camera_pos_ws, "u_camera_pos_ws", final_pass::program)) return false;
	if(!get_uniform(final_pass::inverse_projview, "u_inverse_projview", final_pass::program)) return false;
	if(!get_uniform(final_pass::point_light_color, "u_light_color", final_pass::program)) return false;
	if(!get_uniform(final_pass::point_light_pos, "u_light_pos_ws", final_pass::program)) return false;
	if(!get_uniform(final_pass::depth, "u_depth_buffer", final_pass::program)) return false;
	if(!get_uniform(final_pass::albedo, "u_albedo_buffer", final_pass::program)) return false;
	if(!get_uniform(final_pass::normal, "u_normal_buffer", final_pass::program)) return false;
	if(!get_uniform(final_pass::materials, "u_materials_buffer", final_pass::program)) return false;

	if(!loadProgram(blit_texture::program, "data/shader/blit_texture.vert", "data/shader/blit_texture.frag", error))  {
		cerr << error << endl;
		return false;
	}
	if(!get_attribute(blit_texture::mesh_vertex, "quad_cs", blit_texture::program)) return false;
	if(!get_uniform(blit_texture::position, "position", blit_texture::program)) return false;
	if(!get_uniform(blit_texture::scale, "scale", blit_texture::program)) return false;
	if(!get_uniform(blit_texture::blit_texture, "blit_texture", blit_texture::program)) return false;
	if(!get_uniform(blit_texture::uv_scale, "uv_scale", blit_texture::program)) return false;
	if(!get_uniform(blit_texture::uv_offset, "uv_offset", blit_texture::program)) return false;
	if(!get_uniform(blit_texture::color, "color", blit_texture::program)) return false;

	if(!loadProgram(skybox::program, "data/shader/skybox.vert", "data/shader/skybox.frag", error))  {
		cerr << error << endl;
		return false;
	}
	if(!get_attribute(skybox::mesh_vertex, "cube_os", skybox::program)) return false;
	if(!get_uniform(skybox::proj_view, "proj_view", skybox::program)) return false;
	if(!get_uniform(skybox::skybox, "skybox", skybox::program)) return false;

	return true;
}
void deletePrograms() {
	deleteProgram(core_pass::program);
	deleteProgram(final_pass::program);
	deleteProgram(blit_texture::program);
	deleteProgram(skybox::program);
}
