#ifndef BASE_MESH_HPP
#define BASE_MESH_HPP

#include <lyft/lyft.hpp>
#include <tol/tiny_obj_loader.h>

struct Mesh {
	Vao vao;
	Vbo elements_vbo, vertices_vbo, normals_vbo, tangents_vbo, bitangents_vbo, uvs_vbo;
	// loaded
	vector<uint32> elements;
	vector<vec3> vertices;
	vector<vec3> normals;
	vector<vec2> uvs;
	// generated
	vector<vec3> tangents;
	vector<vec3> bitangents;
};

void createMesh(Mesh& mesh);
void deleteMesh(Mesh& mesh);

bool loadObj(const char* filepath, vector<uint>& mesh_elements, vector<vec3>& mesh_vertices, vector<vec3>& mesh_normals, vector<vec2>& mesh_uvs);
bool loadObjMesh(Mesh& mesh, const char* filepath);

void generateMeshTangents(Mesh& mesh);
void uploadMesh(Mesh& mesh);
void attachMeshAttributes(const Mesh& mesh, Attribute vertex, Attribute normal, Attribute uv, Attribute tangent, Attribute bitangent);
void renderMeshElements(const Mesh& mesh, GLenum mode = GL_TRIANGLES);
void renderMeshVertices(const Mesh& mesh, GLenum mode);

inline bool loadObjMesh(Mesh& mesh, const char* filepath) {
	lftAssert(mesh.vao.handle != GLuint(-1));
	return loadObj(filepath, mesh.elements, mesh.vertices, mesh.normals, mesh.uvs);
}

#endif
