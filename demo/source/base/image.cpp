#include "base/image.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>

bool loadImage(Image& image, int stbi_format, const char* filepath, bool flipy) {
	stbi_set_flip_vertically_on_load(flipy);
	image.data = stbi_load(filepath, &image.width, &image.height, &image.channels, stbi_format);
	if(image.data == nullptr) return false;
	image.format = stbi_format;
	return true;
}
bool loadImageCube(ImageCube& image_cube, int stbi_format, const char* px, const char* nx, const char* py, const char* ny, const char* pz, const char* nz, bool flipy) {
	return
		loadImage(image_cube.px, stbi_format, px, flipy) && loadImage(image_cube.nx, stbi_format, nx, flipy) &&
		loadImage(image_cube.py, stbi_format, py, flipy) && loadImage(image_cube.ny, stbi_format, ny, flipy) &&
		loadImage(image_cube.pz, stbi_format, pz, flipy) && loadImage(image_cube.nz, stbi_format, nz, flipy);
}
void uploadImageTextureCube(ImageCube& image) {
	if(image.px.data != nullptr)
		uploadTextureCube(GL_TEXTURE_CUBE_MAP_POSITIVE_X, image.px.data, image.px.width, image.px.height, GL_UNSIGNED_BYTE, image.px.format == STBI_rgb_alpha ? GL_RGBA : GL_RGB, image.px.format == STBI_rgb_alpha ? GL_RGBA : GL_RGB);
	if(image.nx.data != nullptr)
		uploadTextureCube(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, image.nx.data, image.nx.width, image.nx.height, GL_UNSIGNED_BYTE, image.px.format == STBI_rgb_alpha ? GL_RGBA : GL_RGB, image.px.format == STBI_rgb_alpha ? GL_RGBA : GL_RGB);
	if(image.py.data != nullptr)
		uploadTextureCube(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, image.py.data, image.py.width, image.py.height, GL_UNSIGNED_BYTE, image.px.format == STBI_rgb_alpha ? GL_RGBA : GL_RGB, image.px.format == STBI_rgb_alpha ? GL_RGBA : GL_RGB);
	if(image.ny.data != nullptr)
		uploadTextureCube(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, image.ny.data, image.ny.width, image.ny.height, GL_UNSIGNED_BYTE, image.px.format == STBI_rgb_alpha ? GL_RGBA : GL_RGB, image.px.format == STBI_rgb_alpha ? GL_RGBA : GL_RGB);
	if(image.pz.data != nullptr)
		uploadTextureCube(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, image.pz.data, image.pz.width, image.pz.height, GL_UNSIGNED_BYTE, image.px.format == STBI_rgb_alpha ? GL_RGBA : GL_RGB, image.px.format == STBI_rgb_alpha ? GL_RGBA : GL_RGB);
	if(image.nz.data != nullptr)
		uploadTextureCube(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, image.nz.data, image.nz.width, image.nz.height, GL_UNSIGNED_BYTE, image.px.format == STBI_rgb_alpha ? GL_RGBA : GL_RGB, image.px.format == STBI_rgb_alpha ? GL_RGBA : GL_RGB);
}
