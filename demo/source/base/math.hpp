#ifndef BASE_MATH_HPP
#define BASE_MATH_HPP

#include <lyft/lyft.hpp>

namespace mth {
constexpr real32 epsilon = 1e-8;
constexpr real32 pi = std::acos(-1.0);
const vec3 up = { 0.0, 1.0, 0.0 }, right = { 1.0, 0.0, 0.0 }, forward = { 0.0, 0.0, -1.0 };
}

constexpr real32 radians(real32 deg) {
	return deg * mth::pi / 180.0;
}
constexpr real32 degrees(real32 rad) {
	return rad * 180.0 / mth::pi;
}

bool zero(real32 v);

inline bool zero(real32 v) {
	return std::abs(v) < mth::epsilon;
}

#endif
