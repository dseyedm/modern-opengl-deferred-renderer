#ifndef BASE_MATERIAL_HPP
#define BASE_MATERIAL_HPP

#include "base/image.hpp"

#include <lyft/lyft.hpp>

struct Material {
	Texture2D albedo, normal/*, detail */;
	vec3 color { 1.0 };
	// vec2 detail_scale { 1.0 };

	real32 roughness = 0.3;
	real32 fresnel_reflection = 0.8;
	real32 specular_reflection = 0.8;
};

void createMaterial(Material& mat);
void loadMaterial(Material& mat, Image& albedo_map, Image& normal_map);
void deleteMaterial(Material& mat);

#endif
