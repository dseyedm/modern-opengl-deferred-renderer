#include "base/program.hpp"
#include "base/load_file.hpp"

#include <iostream>

bool loadProgram(Program& program, const char* vert_file, const char* frag_file, string& error) {
	lftAssert(program.handle != GLuint(-1));
	Shader vert, frag;
	createShader(vert, GL_VERTEX_SHADER);
	string vert_error = compileShader(vert, loadFile(vert_file).c_str());
	if(vert_error.size()) {
		error = "failed to load vertex shader \"";
		error += vert_file;
		error += "\":\n";
		error += vert_error;
		deleteShader(vert);
		return false;
	}
	createShader(frag, GL_FRAGMENT_SHADER);
	string frag_error = compileShader(frag, loadFile(frag_file).c_str());
	if(frag_error.size()) {
		error = "failed to load fragment shader \"";
		error += frag_file;
		error += "\":\n";
		error += frag_error;
		deleteShader(vert);
		deleteShader(frag);
		return false;
	}
	string linker_error = linkProgram(program, vert, frag);
	if(linker_error.size()) {
		error = "failed to link program: (\"";
		error += vert_file;
		error += ", \"";
		error += frag_file;
		error += "\")\n";
		error += linker_error;
		deleteShader(vert);
		deleteShader(frag);
		return false;
	}
	deleteShader(vert);
	deleteShader(frag);
	return true;
}
