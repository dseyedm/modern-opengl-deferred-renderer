#include "base/mesh.hpp"
#include "base/math.hpp"

// todo: steal tol loading function and rewrite it using glm
bool loadObj(const char* filepath, vector<uint>& mesh_elements, vector<vec3>& mesh_vertices, vector<vec3>& mesh_normals, vector<vec2>& mesh_uvs) {
	auto toVec3 = [](const vector<float>& v, vector<vec3>& v3) {
		v3.resize(v.size() / 2);
		for(size_t i = 0; i < v.size(); i += 3) {
			v3[i / 3] = vec3(v[i], v[i+1], v[i+2]);
		}
	};
	auto toVec2 = [](const vector<float>& v, vector<vec2>& v2) {
		v2.resize(v.size() / 2);
		for(size_t i = 0; i < v.size(); i += 2) {
			v2[i / 2] = vec2(v[i], v[i+1]);
		}
	};

	vector<tinyobj::shape_t> shapes;
	vector<tinyobj::material_t> materials;
	string error = tinyobj::LoadObj(shapes, materials, filepath);

	if(error.size()) return false;
	if(shapes.empty()) return true;

	mesh_elements = shapes.front().mesh.indices;
	toVec3(shapes.front().mesh.positions, mesh_vertices);
	toVec2(shapes.front().mesh.texcoords, mesh_uvs);
	toVec3(shapes.front().mesh.normals, mesh_normals);
	return true;
}
void createMesh(Mesh& mesh) {
	createVao(mesh.vao);
	createVbo(mesh.elements_vbo);
	createVbo(mesh.vertices_vbo);
	createVbo(mesh.normals_vbo);
	createVbo(mesh.tangents_vbo);
	createVbo(mesh.bitangents_vbo);
	createVbo(mesh.uvs_vbo);
}
void deleteMesh(Mesh& mesh) {
	deleteVao(mesh.vao);
	deleteVbo(mesh.elements_vbo);
	deleteVbo(mesh.vertices_vbo);
	deleteVbo(mesh.normals_vbo);
	deleteVbo(mesh.tangents_vbo);
	deleteVbo(mesh.bitangents_vbo);
	deleteVbo(mesh.uvs_vbo);
}
void generateMeshTangents(Mesh& mesh) {
	lftAssert(mesh.elements.size());
	lftAssert(mesh.vertices.size());
	lftAssert(mesh.uvs.size());

	mesh.tangents.resize(mesh.vertices.size());
	mesh.bitangents.resize(mesh.vertices.size());
	for(uint i = 0; i < mesh.tangents.size(); ++i) {
		mesh.tangents[i] = vec3(0.0);
		mesh.bitangents[i] = vec3(0.0);
	}
	for(uint i = 0; i < mesh.elements.size(); i += 3) {
		vec3& vertex_a = mesh.vertices[mesh.elements[i + 0]];
		vec3& vertex_b = mesh.vertices[mesh.elements[i + 1]];
		vec3& vertex_c = mesh.vertices[mesh.elements[i + 2]];

		vec2& uv_a = mesh.uvs[mesh.elements[i + 0]];
		vec2& uv_b = mesh.uvs[mesh.elements[i + 1]];
		vec2& uv_c = mesh.uvs[mesh.elements[i + 2]];

		vec3 ba = vertex_b - vertex_a;
		vec3 ca = vertex_c - vertex_a;
		vec2 uv_ba = uv_b - uv_a;
		vec2 uv_ca = uv_c - uv_a;

		real32 r = 1.0 / (uv_ba.x * uv_ca.y - uv_ba.y * uv_ca.x);

		vec3 tangent {
			(uv_ca.y * ba.x - uv_ba.y * ca.x) * r,
			(uv_ca.y * ba.y - uv_ba.y * ca.y) * r,
			(uv_ca.y * ba.z - uv_ba.y * ca.z) * r,
		};

		mesh.tangents[mesh.elements[i + 0]] += tangent;
		mesh.tangents[mesh.elements[i + 1]] += tangent;
		mesh.tangents[mesh.elements[i + 2]] += tangent;

		vec3 bitangent {
			(-uv_ca.x * ba.x - uv_ba.x * ca.x) * r,
			(-uv_ca.x * ba.y - uv_ba.x * ca.y) * r,
			(-uv_ca.x * ba.z - uv_ba.x * ca.z) * r,
		};

		mesh.bitangents[mesh.elements[i + 0]] += bitangent;
		mesh.bitangents[mesh.elements[i + 1]] += bitangent;
		mesh.bitangents[mesh.elements[i + 2]] += bitangent;

	}
	for(uint i = 0; i < mesh.tangents.size(); ++i) {
		glm::normalize(mesh.tangents[i]);
		//glm::normalize(mesh.bitangents[i]);
	}
}
void uploadMesh(Mesh& mesh) {
	bindVao(mesh.vao);
	if(mesh.elements.size()) {
		bindElementVbo(mesh.elements_vbo);
		uploadElementVbo(mesh.elements.data(), mesh.elements.size());
		unbindElementVbo();
	}
	if(mesh.vertices.size()) {
		bindArrayVbo(mesh.vertices_vbo);
		uploadArrayVbo(mesh.vertices.data(), mesh.vertices.size());
		unbindArrayVbo();
	}
	if(mesh.normals.size()) {
		bindArrayVbo(mesh.normals_vbo);
		uploadArrayVbo(mesh.normals.data(), mesh.normals.size());
		unbindArrayVbo();
	}
	if(mesh.tangents.size()) {
		bindArrayVbo(mesh.tangents_vbo);
		uploadArrayVbo(mesh.tangents.data(), mesh.tangents.size());
		unbindArrayVbo();
	}
	if(mesh.bitangents.size()) {
		bindArrayVbo(mesh.bitangents_vbo);
		uploadArrayVbo(mesh.bitangents.data(), mesh.bitangents.size());
		unbindArrayVbo();
	}
	if(mesh.uvs.size()) {
		bindArrayVbo(mesh.uvs_vbo);
		uploadArrayVbo(mesh.uvs.data(), mesh.uvs.size());
		unbindArrayVbo();
	}
	unbindVao();
}
void attachMeshAttributes(const Mesh& mesh, Attribute vertex, Attribute normal, Attribute uv, Attribute tangent, Attribute bitangent) {
	bindVao(mesh.vao);
	if(vertex.handle != GLuint(-1)) {
		lftAssert(mesh.vertices.size());
		bindArrayVbo(mesh.vertices_vbo);
		enableAttribute(vertex);
		setAttributePtr(vertex, GL_FLOAT, 3);
		unbindArrayVbo();
	}
	if(normal.handle != GLuint(-1)) {
		lftAssert(mesh.normals.size());
		bindArrayVbo(mesh.normals_vbo);
		enableAttribute(normal);
		setAttributePtr(normal, GL_FLOAT, 3);
		unbindArrayVbo();
	}
	if(tangent.handle != GLuint(-1)) {
		lftAssert(mesh.tangents.size());
		bindArrayVbo(mesh.tangents_vbo);
		enableAttribute(tangent);
		setAttributePtr(tangent, GL_FLOAT, 3);
		unbindArrayVbo();
	}
	if(bitangent.handle != GLuint(-1)) {
		lftAssert(mesh.bitangents.size());
		bindArrayVbo(mesh.bitangents_vbo);
		enableAttribute(bitangent);
		setAttributePtr(bitangent, GL_FLOAT, 3);
		unbindArrayVbo();
	}
	if(uv.handle != GLuint(-1)) {
		lftAssert(mesh.uvs.size());
		bindArrayVbo(mesh.uvs_vbo);
		enableAttribute(uv);
		setAttributePtr(uv, GL_FLOAT, 2);
		unbindArrayVbo();
	}
	unbindVao();
}
void renderMeshElements(const Mesh& mesh, GLenum mode) {
	bindVao(mesh.vao);
	bindElementVbo(mesh.elements_vbo);
	drawElementVbo(mesh.elements.size(), GL_UNSIGNED_INT, mode);
	unbindElementVbo();
	unbindVao();
}
void renderMeshVertices(const Mesh& mesh, GLenum mode) {
	bindVao(mesh.vao);
	bindArrayVbo(mesh.vertices_vbo);
	drawArrayVbo(mesh.vertices.size(), mode);
	unbindArrayVbo();
	unbindVao();
}
