#ifndef BASE_IMAGE_HPP
#define BASE_IMAGE_HPP

#include <lyft/lyft.hpp>
#include <stb/stb_image.h>

struct Image {
	int format, width, height, channels;
	uint8* data = nullptr;
};
bool loadImage(Image& image, int stbi_format /* STBI_rgb_alpha or STBI_rgb */, const char* filepath, bool flipy = true);
void deleteImage(Image& image);
void deleteImages(Image* images, uint sz);
void uploadImageTexture2D(Image& image);

struct ImageCube {
	Image px, nx, py, ny, pz, nz;
};
bool loadImageCube(ImageCube& image_cube, int stbi_format, const char* px, const char* nx, const char* py, const char* ny, const char* pz, const char* nz, bool flipy = true);
void deleteImageCube(ImageCube& image_cube);
void uploadImageTextureCube(ImageCube& image);


inline void deleteImage(Image& image) {
	lftAssert(image.data != nullptr);
	stbi_image_free(image.data);
}
inline void deleteImages(Image* images, uint sz) {
	#ifndef NDEBUG
	for(uint i = 0; i < sz; ++i) {
		lftAssert(images[i].data != nullptr);
	}
	#endif
	for(uint i = 0; i < sz; ++i) {
		stbi_image_free(images[i].data);
	}
}
inline void uploadImageTexture2D(Image& image) {
	lftAssert(image.data != nullptr);
	uploadTexture2D(image.data, image.width, image.height, GL_UNSIGNED_BYTE, image.format == STBI_rgb_alpha ? GL_RGBA : GL_RGB, image.format == STBI_rgb_alpha ? GL_RGBA : GL_RGB);
}
inline void deleteImageCube(ImageCube& image_cube) {
	deleteImages(&image_cube.px, 6);
}

#endif
