#ifndef BASE_LOAD_FILE_HPP
#define BASE_LOAD_FILE_HPP

#include <string>

bool loadFile(const char* filename, std::string& out);
std::string loadFile(const char* filename);

#endif
