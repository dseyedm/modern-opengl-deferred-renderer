#ifndef BASE_PROGRAM_HPP
#define BASE_PROGRAM_HPP

#include <lyft/lyft.hpp>

bool loadProgram(Program& program, const char* vert_file, const char* frag_file, string& error);

#endif
