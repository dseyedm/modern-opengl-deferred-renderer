#ifndef BASE_CAMERA_HPP
#define BASE_CAMERA_HPP

#include "base/math.hpp"
#include "base/transform.hpp"

#include <lyft/lyft.hpp>
#include <glfw/glfw3.h>

struct Camera {
	vec3 pos = vec3(0.0), vel = vec3(0.0), acc = vec3(0.0);
	quat orient;
	real32 fov = radians(90.0), aspect = 800.0 / 600.0, near = 0.1, far = 1000.0;
};
void transformCamera(GLFWwindow* window, Camera& camera, real32 sens_x, real32 sens_y, real32 dts);

mat4 cameraView(const quat& o, const vec3& pos);
mat4 cameraProj(real32 fov, real32 aspect, real32 near, real32 far);

mat4 cameraProj(const Camera& camera);
mat4 cameraView(const Camera& camera);

inline mat4 cameraView(const quat& o, const vec3& pos) {
	return glm::inverse(orientMat4(o)) * translate(mat4(), -pos);
}
inline mat4 cameraProj(real32 fov, real32 aspect, real32 near, real32 far) {
	lftAssert(near < far && !zero(far - near));
	return glm::perspective(fov, aspect, near, far);
}

inline mat4 cameraProj(const Camera& camera) {
	return cameraProj(camera.fov, camera.aspect, camera.near, camera.far);
}
inline mat4 cameraView(const Camera& camera) {
	return cameraView(camera.orient, camera.pos);
}

#endif
