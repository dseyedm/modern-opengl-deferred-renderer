#include "base/camera.hpp"

void transformCamera(GLFWwindow* window, Camera& camera, real32 sens_x, real32 sens_y, real32 dts) {
	// rotation
	bool pressing_mb_2 = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_2);
	static bool pressed_mb_2 = pressing_mb_2;
	static real64 origin_x, origin_y;
	if(pressing_mb_2 && !pressed_mb_2) {
		glfwGetCursorPos(window, &origin_x, &origin_y);
	} else if(pressing_mb_2 && pressed_mb_2) {
		real64 curr_x, curr_y;
		glfwGetCursorPos(window, &curr_x, &curr_y);
		real64 diff_x = curr_x - origin_x;
		real64 diff_y = curr_y - origin_y;
		rotate(camera.orient, radians(-diff_y * sens_y), mth::right);
		rotate(camera.orient, radians(-diff_x * sens_x), localUpInv(camera.orient));
	}
	pressed_mb_2 = pressing_mb_2;
	// movement
	static real32 accel = 10.0, max_velocity = 7.0, friction = 3.0;
	bool key_w = glfwGetKey(window, GLFW_KEY_W),
		key_a = glfwGetKey(window, GLFW_KEY_A),
		key_s = glfwGetKey(window, GLFW_KEY_S),
		key_d = glfwGetKey(window, GLFW_KEY_D),
		key_sp = glfwGetKey(window, GLFW_KEY_SPACE),
		key_ct = glfwGetKey(window, GLFW_KEY_LEFT_CONTROL);
	vec3 local_forward = localForward(camera.orient), local_right = localRight(camera.orient), local_up = localUp(camera.orient);
    vec3 move;
	if(key_w)  move += local_forward;
	if(key_s)  move -= local_forward;
	if(key_d)  move += local_right;
	if(key_a)  move -= local_right;
	if(key_sp) move += local_up;
	if(key_ct) move -= local_up;
	glm::normalize(move);
	camera.acc = move * accel;
    camera.vel += camera.acc * dts;
	vec3 dir = camera.vel;
	if(!zero(glm::length(dir))) {
		glm::normalize(dir);
		if(glm::length(dir * friction * dts) > glm::length(camera.vel)) camera.vel = vec3(0.0);
		else camera.vel -= dir * friction * dts;
	}
	if(glm::length(camera.vel) > max_velocity) camera.vel *= max_velocity / length(camera.vel);
	else if(zero(glm::length(camera.vel))) camera.vel = vec3(0.0);
    camera.pos += camera.vel * dts;
}
