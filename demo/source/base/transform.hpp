#ifndef BASE_TRANSFORM_HPP
#define BASE_TRANSFORM_HPP

#include <lyft/lyft.hpp>

#include "base/math.hpp"

void rotate(quat& o, real32 rads, vec3 axis);
void look(quat& o, const vec3& pos, const vec3& d);
void lookAt(quat& o, const vec3& pos, const vec3& point);

mat4 posMat4(const vec3& pos);
mat4 orientMat4(const quat& o);
mat4 scaleMat4(const vec3& scl);

vec3 localUp(const quat& o);
vec3 localRight(const quat& o);
vec3 localForward(const quat& o);
vec3 localUpInv(const quat& o);
vec3 localRightInv(const quat& o);
vec3 localForwardInv(const quat& o);

inline void rotate(quat& o, real32 rads, vec3 axis) {
	lftAssert(!zero(glm::length(axis)));
	glm::normalize(axis);
	o *= glm::angleAxis(rads, axis);
}
inline void look(quat& o, const vec3& pos, const vec3& d) {
	o = glm::inverse(quat(glm::lookAt(pos, pos + d + mth::epsilon, mth::up)));
}
inline void lookAt(quat& o, const vec3& pos, const vec3& point) {
	look(o, pos, point - pos);
}
inline mat4 posMat4(const vec3& pos) {
	return translate(mat4(1.0), pos);
}
inline mat4 orientMat4(const quat& o) {
	return toMat4(o);
}
inline mat4 scaleMat4(const vec3& scl) {
	return scale(mat4(1.0), scl);
}
inline vec3 localUp(const quat& o) {
	return vec3(orientMat4(o) * vec4(mth::up, 0.0));
}
inline vec3 localRight(const quat& o) {
	return vec3(orientMat4(o) * vec4(mth::right, 0.0));
}
inline vec3 localForward(const quat& o) {
	return vec3(orientMat4(o) * vec4(mth::forward, 0.0));
}
inline vec3 localUpInv(const quat& o) {
	return vec3(glm::inverse(orientMat4(o)) * vec4(mth::up, 0.0));
}
inline vec3 localRightInv(const quat& o) {
	return vec3(glm::inverse(orientMat4(o)) * vec4(mth::right, 0.0));
}
inline vec3 localForwardInv(const quat& o) {
	return vec3(glm::inverse(orientMat4(o)) * vec4(mth::forward, 0.0));
}

#endif
