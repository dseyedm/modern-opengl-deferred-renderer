#include "base/material.hpp"

void createMaterial(Material& mat) {
	createTexture2D(mat.albedo);
	createTexture2D(mat.normal);
}
void loadMaterial(Material& mat, Image& albedo_map, Image& normal_map) {
	bindTexture2D(mat.albedo);
	uploadImageTexture2D(albedo_map);
	unbindTexture2D();
	bindTexture2D(mat.normal);
	uploadImageTexture2D(normal_map);
	unbindTexture2D();
}
void deleteMaterial(Material& mat) {
	deleteTexture2D(mat.albedo);
	deleteTexture2D(mat.normal);
}
