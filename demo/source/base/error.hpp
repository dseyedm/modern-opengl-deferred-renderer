#ifndef BASE_ERROR_HPP
#define BASE_ERROR_HPP

#include <iostream>

void printFailedToLoad(const char* str);
void printFailedToFind(const char* str);

inline void printFailedToLoad(const char* str) {
	std::cout << "failed to load \"" << str << "\"" << std::endl;
}
inline void printFailedToFind(const char* str) {
	std::cout << "failed to find \"" << str << "\"" << std::endl;
}

#endif
