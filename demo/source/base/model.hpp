#ifndef BASE_MODEL_HPP
#define BASE_MODEL_HPP

#include "base/mesh.hpp"
#include "base/material.hpp"
#include "base/transform.hpp"

struct Model {
	const Mesh* msh = nullptr;
	Material mat;
	vec3 pos, vel, acc, scl = vec3(1.0);
	quat orient;
	vec2 uv_scl { 1.0 };
};

// returns the matrix which transforms objects from model space to world space
mat4 modelMat4(const Model& model);

inline mat4 modelMat4(const Model& model) {
	return posMat4(model.pos) * orientMat4(model.orient) * scaleMat4(model.scl);
}

#endif
