#include "base/load_file.hpp"

#include <fstream>
#include <sstream>
using namespace std;

bool loadFile(const char* filename, string& out) {
	fstream fin;
	fin.open(filename);
	if(!fin.is_open()) return false;
	stringstream ss;
	ss << fin.rdbuf();
	out = ss.str();
	return true;
}
string loadFile(const char* filename) {
	fstream fin;
	fin.open(filename);
	if(!fin.is_open()) return string();
	stringstream ss;
	ss << fin.rdbuf();
	return ss.str();
}
