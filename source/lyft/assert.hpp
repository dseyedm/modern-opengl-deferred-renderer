#ifndef LFT_ASSERT
#define LFT_ASSERT

#ifdef NDEBUG
#define lftAssert(c) (void(0))
#else
#define lftAssert(c) ((c) ? (void)0 : _lftAssert(#c, __FILE__, __FUNCTION__, __LINE__))
void _lftAssert(const char* condition, const char* file, const char* func, int line);
#endif

#endif
