#ifndef LYFT_VBO_HPP
#define LYFT_VBO_HPP

#include "lyft/types.hpp"
#include "lyft/context.hpp"
#include "lyft/glm.hpp"

struct Vbo {
	GLuint handle = GLuint(-1);
};

void createVbo(Vbo& vbo);
void createVbos(Vbo* vbos, uint sz);
void deleteVbo(Vbo& vbo);
void deleteVbos(Vbo* vbos, uint sz);

void bindElementVbo(Vbo vbo);
void unbindElementVbo();
Vbo boundElementVbo();
void bindArrayVbo(Vbo vbo);
void unbindArrayVbo();
Vbo boundArrayVbo();

void uploadArrayVbo(real32* data, uint sz);
void uploadArrayVbo(vec2* data, uint sz);
void uploadArrayVbo(vec3* data, uint sz);
void uploadArrayVbo(vec4* data, uint sz);

void uploadElementVbo(uint8* data, uint sz);
void uploadElementVbo(uint16* data, uint sz);
void uploadElementVbo(uint32* data, uint sz);

void drawArrayVbo(uint sz, GLenum mode = GL_TRIANGLES);
void drawElementVbo(uint sz, GLenum ebsize, GLenum mode = GL_TRIANGLES);


inline void createVbo(Vbo& vbo) {
	lftAssert(vbo.handle == GLuint(-1));
	gl(GenBuffers(1, &vbo.handle));
}
inline void createVbos(Vbo* vbos, uint sz) {
    #ifndef NDEBUG
    for(uint i = 0; i < sz; ++i) {
		lftAssert(vbos[i].handle == GLuint(-1));
    }
    #endif
    lftAssert(sz);
	gl(GenBuffers(sz, &vbos[0].handle));
}
inline void deleteVbo(Vbo& vbo) {
	lftAssert(vbo.handle != GLuint(-1));
	gl(DeleteBuffers(1, &vbo.handle));
	vbo.handle = GLuint(-1);
}
inline void deleteVbos(Vbo* vbos, uint sz) {
	#ifndef NDEBUG
	for(uint i = 0; i < sz; ++i) {
		lftAssert(vbos[i].handle != GLuint(-1));
	}
	#endif
	lftAssert(sz);
	gl(DeleteBuffers(sz, &vbos[0].handle));
	for(uint i = 0; i < sz; ++i) {
		vbos[i].handle = GLuint(-1);
	}
}
inline void bindElementVbo(Vbo vbo) {
	lftAssert(vbo.handle != GLuint(-1));
	gl(BindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo.handle));
}
inline void unbindElementVbo() {
	gl(BindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0));
}
inline Vbo boundElementVbo() {
	GLint bound;
	gl(GetIntegerv(GL_ELEMENT_ARRAY_BUFFER_BINDING, &bound));
	Vbo vbo;
	vbo.handle = bound > 0 ? bound : GLuint(-1);
	return vbo;
}
inline void bindArrayVbo(Vbo vbo) {
	lftAssert(vbo.handle != GLuint(-1));
	gl(BindBuffer(GL_ARRAY_BUFFER, vbo.handle));
}
inline void unbindArrayVbo() {
	gl(BindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0));
}
inline Vbo boundArrayVbo() {
	GLint bound;
	gl(GetIntegerv(GL_ARRAY_BUFFER_BINDING, &bound));
	Vbo vbo;
	vbo.handle = bound > 0 ? bound : GLuint(-1);
	return vbo;
}
inline void uploadArrayVbo(real32* data, uint sz) {
	lftAssert(boundArrayVbo().handle != GLuint(-1));
	gl(BufferData(GL_ARRAY_BUFFER, sz * sizeof(real32), data, GL_STATIC_DRAW));
}
inline void uploadArrayVbo(vec2* data, uint sz) {
	lftAssert(boundArrayVbo().handle != GLuint(-1));
	gl(BufferData(GL_ARRAY_BUFFER, sz * sizeof(vec2), data, GL_STATIC_DRAW));
}
inline void uploadArrayVbo(vec3* data, uint sz) {
	lftAssert(boundArrayVbo().handle != GLuint(-1));
	gl(BufferData(GL_ARRAY_BUFFER, sz * sizeof(vec3), data, GL_STATIC_DRAW));
}
inline void uploadArrayVbo(vec4* data, uint sz) {
	lftAssert(boundArrayVbo().handle != GLuint(-1));
	gl(BufferData(GL_ARRAY_BUFFER, sz * sizeof(vec4), data, GL_STATIC_DRAW));
}
inline void uploadElementVbo(uint8* data, uint sz) {
	lftAssert(boundElementVbo().handle != GLuint(-1));
	gl(BufferData(GL_ELEMENT_ARRAY_BUFFER, sz * sizeof(uint8), data, GL_STATIC_DRAW));
}
inline void uploadElementVbo(uint16* data, uint sz) {
	lftAssert(boundElementVbo().handle != GLuint(-1));
	gl(BufferData(GL_ELEMENT_ARRAY_BUFFER, sz * sizeof(uint16), data, GL_STATIC_DRAW));
}
inline void uploadElementVbo(uint32* data, uint sz) {
	lftAssert(boundElementVbo().handle != GLuint(-1));
	gl(BufferData(GL_ELEMENT_ARRAY_BUFFER, sz * sizeof(uint32), data, GL_STATIC_DRAW));
}
inline void drawArrayVbo(uint sz, GLenum mode) {
	gl(DrawArrays(mode, 0, sz));
}
inline void drawElementVbo(uint sz, GLenum ebsize, GLenum mode) {
	gl(DrawElements(mode, sz, ebsize, (void*)0));
}

#endif
