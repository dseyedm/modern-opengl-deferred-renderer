#ifndef LYFT_VAO_HPP
#define LYFT_VAO_HPP

#include "lyft/types.hpp"
#include "lyft/context.hpp"

struct Vao {
	GLuint handle = GLuint(-1);
};

void createVao(Vao& vao);
void createVaos(Vao* vaos, uint sz);

void deleteVao(Vao& vao);
void deleteVaos(Vao* vaos, uint sz);

void bindVao(Vao vao);
void unbindVao();
Vao boundVao();


inline void createVao(Vao& vao) {
	lftAssert(vao.handle == GLuint(-1));
	gl(GenVertexArrays(1, &vao.handle));
}
inline void createVaos(Vao* vaos, uint sz) {
	#ifndef NDEBUG
	for(uint i = 0; i < sz; ++i) {
		lftAssert(vaos[i].handle == GLuint(-1));
	}
	#endif
	lftAssert(sz);
	gl(GenVertexArrays(sz, &vaos[0].handle));
}
inline void deleteVao(Vao& vao) {
	lftAssert(vao.handle != GLuint(-1));
	gl(DeleteVertexArrays(1, &vao.handle));
	vao.handle = GLuint(-1);
}
inline void deleteVaos(Vao* vaos, uint sz) {
	#ifndef NDEBUG
	for(uint i = 0; i < sz; ++i) {
		lftAssert(vaos[i].handle != GLuint(-1));
	}
	#endif
	lftAssert(sz);
	gl(DeleteVertexArrays(sz, &vaos[0].handle));
}
inline void bindVao(Vao vao) {
	lftAssert(vao.handle != GLuint(-1));
	gl(BindVertexArray(vao.handle));
}
inline void unbindVao() {
	gl(BindVertexArray(0));
}
inline Vao boundVao() {
	GLint bound;
	gl(GetIntegerv(GL_VERTEX_ARRAY_BINDING, &bound));
	Vao vao;
	vao.handle = bound > 0 ? bound : GLuint(-1);
	return vao;
}

#endif
