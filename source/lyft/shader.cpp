#include "lyft/shader.hpp"
#include "lyft/vertex_array.hpp"

#include <vector>

string compileShader(Shader shader, const char* source) {
	lftAssert(shader.handle != GLuint(-1));
	glShaderSource(shader.handle, 1, &source, nullptr);
	gl(CompileShader(shader.handle));
	GLint compiled = GL_FALSE;
	glGetShaderiv(shader.handle, GL_COMPILE_STATUS, &compiled);
	if(compiled != GL_TRUE) {
		GLint info_len;
		glGetShaderiv(shader.handle, GL_INFO_LOG_LENGTH, &info_len);
		vector<GLchar> info(info_len);
		glGetShaderInfoLog(shader.handle, info_len, nullptr, info.data());
		return string(info.data(), info_len);
	}
	return "";
}
string getShaderSource(Shader shader) {
	lftAssert(shader.handle != GLuint(-1));
	GLsizei source_len;
	gl(GetShaderiv(shader.handle, GL_SHADER_SOURCE_LENGTH, &source_len));
	vector<GLchar> source(source_len);
	gl(GetShaderSource(shader.handle, source_len, nullptr, source.data()));
	return string(source.data(), source_len);
}

string linkProgram(Program program, Shader vertex, Shader fragment) {
	lftAssert(program.handle != GLuint(-1));
	gl(AttachShader(program.handle, vertex.handle));
	gl(AttachShader(program.handle, fragment.handle));
	gl(LinkProgram(program.handle));
	GLint linked = GL_FALSE;
	gl(GetProgramiv(program.handle, GL_LINK_STATUS, &linked));
	if(linked != GL_TRUE) {
		GLint length;
		glGetProgramiv(program.handle, GL_INFO_LOG_LENGTH, &length);
		vector<GLchar> info(length);
		glGetProgramInfoLog(program.handle, length, nullptr, info.data());
		gl(DetachShader(program.handle, vertex.handle));
		gl(DetachShader(program.handle, fragment.handle));
		return string(info.begin(), info.end());
	}
	gl(DetachShader(program.handle, vertex.handle));
	gl(DetachShader(program.handle, fragment.handle));
	return "";
}
string linkProgram(Program program, Shader vertex, Shader fragment, Shader geometry) {
	lftAssert(program.handle != GLuint(-1));
	gl(AttachShader(program.handle, geometry.handle));
	string error = linkProgram(program, vertex, fragment);
	gl(DetachShader(program.handle, geometry.handle));
	return error;
}

vector<pair<string, Uniform> > getUniforms(Program program) {
	lftAssert(program.handle != GLuint(-1));
	GLint count, max_len;
	gl(GetProgramiv(program.handle, GL_ACTIVE_UNIFORMS, &count));
	gl(GetProgramiv(program.handle, GL_ACTIVE_UNIFORM_MAX_LENGTH, &max_len));
	vector<pair<string, Uniform> > uniforms(count);
	vector<GLchar> name(max_len);
	for(GLint i = 0; i < count; ++i) {
		GLsizei length;
		GLint size;
		GLenum type;
		gl(GetActiveUniform(program.handle, i, name.size(), &length, &size, &type, name.data()));
		uniforms[i].first = string(name.data(), length);
		uniforms[i].second.handle = gl(GetUniformLocation(program.handle, uniforms[i].first.c_str()));
	}
	return uniforms;
}
vector<pair<string, Attribute> > getAttributes(Program program) {
	lftAssert(program.handle != GLuint(-1));
	GLint count, max_len;
	gl(GetProgramiv(program.handle, GL_ACTIVE_ATTRIBUTES, &count));
	gl(GetProgramiv(program.handle, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &max_len));
	vector<pair<string, Attribute> > attributes(count);
	vector<GLchar> name(max_len);
	for(GLint i = 0; i < count; ++i) {
		GLsizei length;
		GLint size;
		GLenum type;
		gl(GetActiveAttrib(program.handle, i, name.size(), &length, &size, &type, name.data()));
		attributes[i].first = string(name.data(), length);
		attributes[i].second.handle = gl(GetAttribLocation(program.handle, attributes[i].first.c_str()));
	}
	return attributes;
}
