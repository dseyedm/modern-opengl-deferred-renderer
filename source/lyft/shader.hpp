#ifndef LFT_SHADER_HPP
#define LFT_SHADER_HPP

#include "lyft/types.hpp"
#include "lyft/context.hpp"
#include "lyft/glm.hpp"
#include "lyft/vertex_array.hpp"

struct Shader {
	GLuint handle = GLuint(-1);
};

void createShader(Shader& shader, GLenum type);
void deleteShader(Shader& shader);

string compileShader(Shader shader, const char* source);
string getShaderSource(Shader shader);

struct Program {
	GLuint handle = GLuint(-1);
};

void createProgram(Program& program);
void deleteProgram(Program& program);

void setAttributeHandle(Program program, const char* attribute, uint i);

string linkProgram(Program program, Shader vertex, Shader fragment);
string linkProgram(Program program, Shader vertex, Shader fragment, Shader geometry);

void bindProgram(Program program);
void unbindProgram();

struct Uniform {
	GLuint handle = GLuint(-1);
};
struct Attribute {
	GLuint handle = GLuint(-1);
};

vector<pair<string, Uniform> > getUniforms(Program program);
vector<pair<string, Attribute> > getAttributes(Program program);
Uniform getUniform(Program program, const char* id);
Attribute getAttribute(Program program, const char* id);

void enableAttribute(Attribute attribute);
void disableAttribute(Attribute attribute);

void setAttributePtr(Attribute attribute, GLenum type, uint32 components);
void setAttribute(Attribute attribute, real32 v);
void setAttribute(Attribute attribute, const vec2& v);
void setAttribute(Attribute attribute, const vec3& v);
void setAttribute(Attribute attribute, const vec4& v);

void setUniform(Uniform uniform, int v);
void setUniform(Uniform uniform, const ivec2& v);
void setUniform(Uniform uniform, const ivec3& v);
void setUniform(Uniform uniform, const ivec4& v);

void setUniform(Uniform uniform, uint v);
void setUniform(Uniform uniform, const uvec2& v);
void setUniform(Uniform uniform, const uvec3& v);
void setUniform(Uniform uniform, const uvec4& v);

void setUniform(Uniform uniform, real32 v);
void setUniform(Uniform uniform, const real32* v, uint sz);

void setUniform(Uniform uniform, const vec2& v);
void setUniform(Uniform uniform, const vec2* v, uint sz);
void setUniform(Uniform uniform, const vec3& v);
void setUniform(Uniform uniform, const vec3* v, uint sz);
void setUniform(Uniform uniform, const vec4& v);
void setUniform(Uniform uniform, const vec4* v, uint sz);

void setUniform(Uniform uniform, const mat2& v);
void setUniform(Uniform uniform, const mat2* v, uint sz);
void setUniform(Uniform uniform, const mat2x3& v);
void setUniform(Uniform uniform, const mat2x3* v, uint sz);
void setUniform(Uniform uniform, const mat2x4& v);
void setUniform(Uniform uniform, const mat2x4* v, uint sz);
void setUniform(Uniform uniform, const mat3& v);
void setUniform(Uniform uniform, const mat3* v, uint sz);
void setUniform(Uniform uniform, const mat3x2& v);
void setUniform(Uniform uniform, const mat3x2* v, uint sz);
void setUniform(Uniform uniform, const mat3x4& v);
void setUniform(Uniform uniform, const mat3x4* v, uint sz);
void setUniform(Uniform uniform, const mat4& v);
void setUniform(Uniform uniform, const mat4* v, uint sz);
void setUniform(Uniform uniform, const mat4x3& v);
void setUniform(Uniform uniform, const mat4x3* v, uint sz);
void setUniform(Uniform uniform, const mat4x2& v);
void setUniform(Uniform uniform, const mat4x2* v, uint sz);


inline void createShader(Shader& shader, GLenum type) {
	lftAssert(shader.handle == GLuint(-1));
	shader.handle = gl(CreateShader(type));
}
inline void deleteShader(Shader& shader) {
	lftAssert(shader.handle != GLuint(-1));
	gl(DeleteShader(shader.handle));
	shader.handle = GLuint(-1);
}
inline void createProgram(Program& program) {
	lftAssert(program.handle == GLuint(-1));
	program.handle = gl(CreateProgram());
}
inline void deleteProgram(Program& program) {
	lftAssert(program.handle != GLuint(-1));
	gl(DeleteProgram(program.handle));
	program.handle = GLuint(-1);
}
inline void setAttributeHandle(Program program, const char* attribute, uint i) {
	lftAssert(program.handle != GLuint(-1));
	gl(BindAttribLocation(program.handle, i, attribute));
}
inline void bindProgram(Program program) {
	lftAssert(program.handle != GLuint(-1));
	gl(UseProgram(program.handle));
}
inline void unbindProgram() {
	gl(UseProgram(0));
}
inline Uniform getUniform(Program program, const char* id) {
	lftAssert(program.handle != GLuint(-1));
	Uniform unif;
	unif.handle = gl(GetUniformLocation(program.handle, id));
	return unif;
}
inline Attribute getAttribute(Program program, const char* id) {
	lftAssert(program.handle != GLuint(-1));
	Attribute attr;
	attr.handle = gl(GetAttribLocation(program.handle, id));
	return attr;
}
inline void enableAttribute(Attribute attribute) {
	lftAssert(boundVao().handle != GLuint(-1));
	gl(EnableVertexAttribArray(attribute.handle));
}
inline void disableAttribute(Attribute attribute) {
	lftAssert(boundVao().handle != GLuint(-1));
	gl(DisableVertexAttribArray(attribute.handle));
}
inline void setAttributePtr(Attribute attribute, GLenum type, uint32 components) {
	lftAssert(attribute.handle != GLuint(-1));
	gl(VertexAttribPointer(attribute.handle, components, type, false, 0, (GLvoid*)nullptr));
}
inline void setAttribute(Attribute attribute, real32 v) {
	lftAssert(attribute.handle != GLuint(-1));
	gl(VertexAttrib1f(attribute.handle, v));
}
inline void setAttribute(Attribute attribute, const vec2& v) {
	lftAssert(attribute.handle != GLuint(-1));
	gl(VertexAttrib2f(attribute.handle, v.x, v.y));
}
inline void setAttribute(Attribute attribute, const vec3& v) {
	lftAssert(attribute.handle != GLuint(-1));
	gl(VertexAttrib3f(attribute.handle, v.x, v.y, v.z));
}
inline void setAttribute(Attribute attribute, const vec4& v) {
	lftAssert(attribute.handle != GLuint(-1));
	gl(VertexAttrib4f(attribute.handle, v.x, v.y, v.z, v.w));
}
inline void setUniform(Uniform uniform, int v) {
	lftAssert(uniform.handle != GLuint(-1));
	gl(Uniform1i(uniform.handle, v));
}
inline void setUniform(Uniform uniform, const ivec2& v) {
	lftAssert(uniform.handle != GLuint(-1));
	gl(Uniform2i(uniform.handle, v.x, v.y));
}
inline void setUniform(Uniform uniform, const ivec3& v) {
	lftAssert(uniform.handle != GLuint(-1));
	gl(Uniform3i(uniform.handle, v.x, v.y, v.z));
}
inline void setUniform(Uniform uniform, const ivec4& v) {
	lftAssert(uniform.handle != GLuint(-1));
	gl(Uniform4i(uniform.handle, v.x, v.y, v.z, v.w));
}
inline void setUniform(Uniform uniform, uint v) {
	lftAssert(uniform.handle != GLuint(-1));
	gl(Uniform1ui(uniform.handle, v));
}
inline void setUniform(Uniform uniform, const uvec2& v) {
	lftAssert(uniform.handle != GLuint(-1));
	gl(Uniform2ui(uniform.handle, v.x, v.y));
}
inline void setUniform(Uniform uniform, const uvec3& v) {
	lftAssert(uniform.handle != GLuint(-1));
	gl(Uniform3ui(uniform.handle, v.x, v.y, v.z));
}
inline void setUniform(Uniform uniform, const uvec4& v) {
	lftAssert(uniform.handle != GLuint(-1));
	gl(Uniform4ui(uniform.handle, v.x, v.y, v.z, v.w));
}
inline void setUniform(Uniform uniform, real32 v) {
	lftAssert(uniform.handle != GLuint(-1));
	gl(Uniform1f(uniform.handle, v));
}
inline void setUniform(Uniform uniform, const real32* v, uint sz) {
	lftAssert(uniform.handle != GLuint(-1));
	gl(Uniform1fv(uniform.handle, sz, v));
}
inline void setUniform(Uniform uniform, const vec2& v) {
	lftAssert(uniform.handle != GLuint(-1));
	gl(Uniform2f(uniform.handle, v.x, v.y));
}
inline void setUniform(Uniform uniform, const vec2* v, uint sz) {
	lftAssert(uniform.handle != GLuint(-1));
	gl(Uniform2fv(uniform.handle, sz, value_ptr(*v)));
}
inline void setUniform(Uniform uniform, const vec3& v) {
	lftAssert(uniform.handle != GLuint(-1));
	gl(Uniform3f(uniform.handle, v.x, v.y, v.z));
}
inline void setUniform(Uniform uniform, const vec3* v, uint sz) {
	lftAssert(uniform.handle != GLuint(-1));
	gl(Uniform3fv(uniform.handle, sz, value_ptr(*v)));
}
inline void setUniform(Uniform uniform, const vec4& v) {
	lftAssert(uniform.handle != GLuint(-1));
	gl(Uniform4f(uniform.handle, v.x, v.y, v.z, v.w));
}
inline void setUniform(Uniform uniform, const vec4* v, uint sz) {
	lftAssert(uniform.handle != GLuint(-1));
	gl(Uniform4fv(uniform.handle, sz, value_ptr(*v)));
}
inline void setUniform(Uniform uniform, const mat2& v) {
	lftAssert(uniform.handle != GLuint(-1));
	gl(UniformMatrix2fv(uniform.handle, 1, false, value_ptr(v)));
}
inline void setUniform(Uniform uniform, const mat2* v, uint sz) {
	lftAssert(uniform.handle != GLuint(-1));
	gl(UniformMatrix2fv(uniform.handle, sz, false, value_ptr(*v)));
}
inline void setUniform(Uniform uniform, const mat2x3& v) {
	lftAssert(uniform.handle != GLuint(-1));
	gl(UniformMatrix2x3fv(uniform.handle, 1, false, value_ptr(v)));
}
inline void setUniform(Uniform uniform, const mat2x3* v, uint sz) {
	lftAssert(uniform.handle != GLuint(-1));
	gl(UniformMatrix2x3fv(uniform.handle, sz, false, value_ptr(*v)));
}
inline void setUniform(Uniform uniform, const mat2x4& v) {
	lftAssert(uniform.handle != GLuint(-1));
	gl(UniformMatrix2x4fv(uniform.handle, 1, false, value_ptr(v)));
}
inline void setUniform(Uniform uniform, const mat2x4* v, uint sz) {
	lftAssert(uniform.handle != GLuint(-1));
	gl(UniformMatrix2x4fv(uniform.handle, sz, false, value_ptr(*v)));
}
inline void setUniform(Uniform uniform, const mat3& v) {
	lftAssert(uniform.handle != GLuint(-1));
	gl(UniformMatrix3fv(uniform.handle, 1, false, value_ptr(v)));
}
inline void setUniform(Uniform uniform, const mat3* v, uint sz) {
	lftAssert(uniform.handle != GLuint(-1));
	gl(UniformMatrix3fv(uniform.handle, sz, false, value_ptr(*v)));
}
inline void setUniform(Uniform uniform, const mat3x2& v) {
	lftAssert(uniform.handle != GLuint(-1));
	gl(UniformMatrix3x2fv(uniform.handle, 1, false, value_ptr(v)));
}
inline void setUniform(Uniform uniform, const mat3x2* v, uint sz) {
	lftAssert(uniform.handle != GLuint(-1));
	gl(UniformMatrix3x2fv(uniform.handle, sz, false, value_ptr(*v)));
}
inline void setUniform(Uniform uniform, const mat3x4& v) {
	lftAssert(uniform.handle != GLuint(-1));
	gl(UniformMatrix3x4fv(uniform.handle, 1, false, value_ptr(v)));
}
inline void setUniform(Uniform uniform, const mat3x4* v, uint sz) {
	lftAssert(uniform.handle != GLuint(-1));
	gl(UniformMatrix3x4fv(uniform.handle, sz, false, value_ptr(*v)));
}
inline void setUniform(Uniform uniform, const mat4& v) {
	lftAssert(uniform.handle != GLuint(-1));
	gl(UniformMatrix4fv(uniform.handle, 1, false, value_ptr(v)));
}
inline void setUniform(Uniform uniform, const mat4* v, uint sz) {
	lftAssert(uniform.handle != GLuint(-1));
	gl(UniformMatrix4fv(uniform.handle, sz, false, value_ptr(*v)));
}
inline void setUniform(Uniform uniform, const mat4x3& v) {
	lftAssert(uniform.handle != GLuint(-1));
	gl(UniformMatrix4x3fv(uniform.handle, 1, false, value_ptr(v)));
}
inline void setUniform(Uniform uniform, const mat4x3* v, uint sz) {
	lftAssert(uniform.handle != GLuint(-1));
	gl(UniformMatrix4x3fv(uniform.handle, sz, false, value_ptr(*v)));
}
inline void setUniform(Uniform uniform, const mat4x2& v) {
	lftAssert(uniform.handle != GLuint(-1));
	gl(UniformMatrix4x2fv(uniform.handle, 1, false, value_ptr(v)));
}
inline void setUniform(Uniform uniform, const mat4x2* v, uint sz) {
	lftAssert(uniform.handle != GLuint(-1));
	gl(UniformMatrix4x2fv(uniform.handle, sz, false, value_ptr(*v)));
}

#endif
