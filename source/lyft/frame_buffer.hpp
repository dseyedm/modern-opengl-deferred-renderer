#ifndef LYFT_FRAMEBUFFER_HPP
#define LYFT_FRAMEBUFFER_HPP

#include "lyft/context.hpp"
#include "lyft/texture.hpp"
#include "lyft/render_buffer.hpp"

struct Fbo {
	GLuint handle = GLuint(-1);
};

void createFbo(Fbo& fbo);
void createFbos(Fbo* fbos, uint sz);

void deleteFbo(Fbo& fbo);
void deleteFbos(Fbo* fbos, uint sz);

void bindFbo(Fbo fbo, GLenum mode = GL_FRAMEBUFFER);
void unbindFbo(GLenum mode = GL_FRAMEBUFFER);
Fbo boundFbo();

void attachTexture2DFbo(GLenum attachment, Texture2D tex, GLenum fbo_mode = GL_FRAMEBUFFER);
void attachTextureCubeFbo(GLenum attachment, GLenum face, TextureCube tex, GLenum fbo_mode = GL_FRAMEBUFFER);
void attachRboFbo(GLenum attachment, GLenum rbo_mode, Rbo rbo, GLenum fbo_mode = GL_FRAMEBUFFER);

void detachTexture2DFbo(GLenum attachment, GLenum fbo_mode = GL_FRAMEBUFFER);
void detachTextureCubeFbo(GLenum attachment, GLenum face, GLenum fbo_mode = GL_FRAMEBUFFER);
void detachRboFbo(GLenum attachment, GLenum fbo_mode = GL_FRAMEBUFFER,  GLenum rbo_mode = GL_RENDERBUFFER);

void setDrawBuffersFbo(GLenum* draw_buffers, uint sz);
void setReadBufferFbo(GLenum read_buffer);

GLenum getStatusFbo(GLenum fbo_mode = GL_FRAMEBUFFER);


inline void createFbo(Fbo& fbo) {
	lftAssert(fbo.handle == GLuint(-1));
	gl(GenFramebuffers(1, &fbo.handle));
}
inline void createFbos(Fbo* fbos, uint sz) {
	#ifndef NDEBUG
	for(uint i = 0; i < sz; ++i) {
		lftAssert(fbos[i].handle == GLuint(-1));
	}
	#endif
	lftAssert(sz);
	gl(GenFramebuffers(sz, &fbos[0].handle));
}

inline void deleteFbo(Fbo& fbo) {
	lftAssert(fbo.handle != GLuint(-1));
	gl(DeleteFramebuffers(1, &fbo.handle));
	fbo.handle = GLuint(-1);
}
inline void deleteFbos(Fbo* fbos, uint sz) {
	#ifndef NDEBUG
	for(uint i = 0; i < sz; ++i) {
		lftAssert(fbos[i].handle == GLuint(-1));
	}
	#endif
	lftAssert(sz);
	gl(DeleteFramebuffers(sz, &fbos[0].handle));
	for(uint i = 0; i < sz; ++i) {
		fbos[i].handle = GLuint(-1);
	}
}

inline void bindFbo(Fbo fbo, GLenum mode) {
	lftAssert(fbo.handle != GLuint(-1));
	gl(BindFramebuffer(mode, fbo.handle));
}
inline void unbindFbo(GLenum mode) {
	lftAssert(boundFbo().handle != 0);
	gl(BindFramebuffer(mode, 0));
}
inline Fbo boundFbo() {
	GLint bound;
	gl(GetIntegerv(GL_FRAMEBUFFER_BINDING, &bound));
	Fbo fbo;
	fbo.handle = bound; // 0 means the default fbo
	return fbo;
}

inline void attachTexture2DFbo(GLenum attachment, Texture2D tex, GLenum fbo_mode) {
	lftAssert(boundFbo().handle != 0);
	gl(FramebufferTexture2D(fbo_mode, attachment, GL_TEXTURE_2D, tex.handle, 0));
}
inline void attachTextureCubeFbo(GLenum attachment, GLenum face, TextureCube tex, GLenum fbo_mode) {
	lftAssert(boundFbo().handle != 0);
	gl(FramebufferTexture2D(fbo_mode, attachment, face, tex.handle, 0));
}
inline void attachRboFbo(GLenum attachment, GLenum rbo_mode, Rbo rbo, GLenum fbo_mode) {
	lftAssert(boundFbo().handle != 0);
	gl(FramebufferRenderbuffer(fbo_mode, attachment, rbo_mode, rbo.handle));
}

inline void detachTexture2DFbo(GLenum attachment, GLenum fbo_mode) {
	lftAssert(boundFbo().handle != 0);
	gl(FramebufferTexture2D(fbo_mode, attachment, GL_TEXTURE_2D, 0, 0));
}
inline void detachTextureCubeFbo(GLenum attachment, GLenum face, GLenum fbo_mode) {
	lftAssert(boundFbo().handle != 0);
	gl(FramebufferTexture2D(fbo_mode, attachment, face, 0, 0));
}
inline void detachRboFbo(GLenum attachment, GLenum fbo_mode, GLenum rbo_mode) {
	lftAssert(boundFbo().handle != 0);
	gl(FramebufferRenderbuffer(fbo_mode, attachment, rbo_mode, 0));
}

inline void setDrawBuffersFbo(GLenum* draw_buffers, uint sz) {
	//lftAssert(boundFbo().handle != 0);
	gl(DrawBuffers(sz, draw_buffers));
}
inline void setReadBufferFbo(GLenum read_buffer) {
	gl(ReadBuffer(read_buffer));
}

inline GLenum getStatusFbo(GLenum fbo_mode) {
	lftAssert(boundFbo().handle != 0);
	return glCheckFramebufferStatus(fbo_mode);
}

#endif
