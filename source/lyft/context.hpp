#ifndef LFT_CONTEXT_HPP
#define LFT_CONTEXT_HPP

// opengl 3.3 glsl 330
#include <gl3w/gl3w.h>

#ifdef gl
#error this macro is used by lyft to check for opengl errors after every gl call
#else
#include "lyft/assert.hpp"
#endif

#ifdef NDEBUG

#define gl(OPENGL_CALL) \
gl##OPENGL_CALL

#else

#define gl(OPENGL_CALL) \
gl##OPENGL_CALL; \
lftAssert(glGetError() == GL_NO_ERROR)

#endif

#endif
