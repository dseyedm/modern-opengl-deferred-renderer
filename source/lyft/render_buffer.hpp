#ifndef LYFT_RBO_HPP
#define LYFT_RBO_HPP

#include "lyft/types.hpp"
#include "lyft/context.hpp"

struct Rbo {
	GLuint handle = GLuint(-1);
};

void createRbo(Rbo& rbo);
void createRbos(Rbo* rbos, uint sz);

void deleteRbo(Rbo& rbo);
void deleteRbos(Rbo* rbos, uint sz);

void bindRbo(Rbo rbo, GLenum mode = GL_RENDERBUFFER);
void unbindRbo(GLenum mode = GL_RENDERBUFFER);
Rbo boundRbo();

void reserveRbo(uint width, uint height, GLenum internal_format);
void reserveRbo(uint width, uint height, GLenum internal_format, uint samples);


inline void createRbo(Rbo& rbo) {
	lftAssert(rbo.handle == GLuint(-1));
	gl(GenRenderbuffers(1, &rbo.handle));
}
inline void createRbos(Rbo* rbos, uint sz) {
	#ifndef NDEBUG
	for(uint i = 0; i < sz; ++i) {
		lftAssert(rbos[i].handle == GLuint(-1));
	}
	#endif
	lftAssert(sz);
	gl(GenRenderbuffers(sz, &rbos[0].handle));
}

inline void deleteRbo(Rbo& rbo) {
	lftAssert(rbo.handle != GLuint(-1));
	gl(DeleteRenderbuffers(1, &rbo.handle));
	rbo.handle = GLuint(-1);
}
inline void deleteRbos(Rbo* rbos, uint sz) {
	#ifndef NDEBUG
	for(uint i = 0; i < sz; ++i) {
		lftAssert(rbos[i].handle == GLuint(-1));
	}
	#endif
	lftAssert(sz);
	gl(DeleteRenderbuffers(sz, &rbos[0].handle));
	for(uint i = 0; i < sz; ++i) {
		rbos[i].handle = GLuint(-1);
	}
}

inline void bindRbo(GLenum mode, Rbo rbo) {
	lftAssert(rbo.handle != GLuint(-1));
	gl(BindRenderbuffer(mode, rbo.handle));
}
inline void unbindRbo(GLenum mode) {
	gl(BindRenderbuffer(mode, 0));
}
inline Rbo boundRbo() {
	GLint bound;
	gl(GetIntegerv(GL_RENDERBUFFER_BINDING, &bound));
	Rbo rbo;
	rbo.handle = bound > 0 ? bound : GLuint(-1);
	return rbo;
}

inline void reserveRbo(GLenum rbo_mode, uint width, uint height, GLenum internal_format) {
	lftAssert(boundRbo().handle != GLuint(-1));
	gl(RenderbufferStorage(rbo_mode, internal_format, width, height));
}
inline void reserveRbo(GLenum rbo_mode, uint width, uint height, GLenum internal_format, uint samples) {
	lftAssert(boundRbo().handle != GLuint(-1));
	gl(RenderbufferStorageMultisample(rbo_mode, samples, internal_format, width, height));
}

#endif
