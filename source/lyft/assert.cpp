#ifndef NDEBUG

#include "assert.hpp"

#include <iostream>

void _lftAssert(const char* condition, const char* file, const char* func, int line) {
	std::cerr << "condition " << condition << " failed in function \"" << func << "\" on line " << line << " in " << file << std::endl;
	exit(1);
}

#endif
