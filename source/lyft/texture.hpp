#ifndef LYFT_TEXTURE_HPP
#define LYFT_TEXTURE_HPP

#include "lyft/types.hpp"
#include "lyft/context.hpp"

void activeTexture(uint unit);

struct Texture2D {
	GLuint handle = GLuint(-1);
};

void createTexture2D(Texture2D& tex);
void createTexture2Ds(Texture2D* texs, uint sz);

void deleteTexture2D(Texture2D& tex);
void deleteTexture2Ds(Texture2D* texs, uint sz);

void bindTexture2D(Texture2D tex);
void bindTexture2D(Texture2D tex, uint unit);
void unbindTexture2D();
void unbindTexture2D(uint unit);
Texture2D boundTexture2D();

void uploadTexture2D(const void* data, uint width, uint height, GLenum type, GLenum format, GLenum internal_format);

void setFilterTexture2D(GLenum filter_mode);
void setWrapTexture2D(GLenum wrap_mode);

struct TextureCube {
	GLuint handle = GLuint(-1);
};

void createTextureCube(TextureCube& tex);
void createTextureCubes(TextureCube* texs, uint sz);

void deleteTextureCube(TextureCube& tex);
void deleteTextureCubes(TextureCube* texs, uint sz);

void bindTextureCube(TextureCube tex);
void bindTextureCube(TextureCube tex, uint unit);
void unbindTextureCube();
TextureCube boundTextureCube();

void uploadTextureCube(GLenum target, const void* data, uint width, uint height, GLenum type, GLenum format, GLenum internal_format);

void setFilterTextureCube(GLenum filter_mode);
void setWrapTextureCube(GLenum wrap_mode);


inline void activeTexture(uint unit) {
	gl(ActiveTexture(GL_TEXTURE0 + unit));
}

inline void createTexture2D(Texture2D& tex) {
	lftAssert(tex.handle == GLuint(-1));
	gl(GenTextures(1, &tex.handle));
}
inline void createTexture2Ds(Texture2D* texs, uint sz) {
	#ifndef NDEBUG
	for(uint i = 0; i < sz; ++i) {
		lftAssert(texs[i].handle == GLuint(-1));
	}
	#endif
	lftAssert(sz);
	gl(GenTextures(sz, &texs[0].handle));
}

inline void deleteTexture2D(Texture2D& tex) {
	lftAssert(tex.handle != GLuint(-1));
	gl(DeleteTextures(1, &tex.handle));
	tex.handle = GLuint(-1);
}
inline void deleteTexture2Ds(Texture2D* texs, uint sz) {
	#ifndef NDEBUG
	for(uint i = 0; i < sz; ++i) {
		lftAssert(texs[i].handle != GLuint(-1));
	}
	#endif
	lftAssert(sz);
	gl(DeleteTextures(sz, &texs[0].handle));
	for(uint i = 0; i < sz; ++i) {
		texs[i].handle = GLuint(-1);
	}
}
inline void bindTexture2D(Texture2D tex) {
	lftAssert(tex.handle != GLuint(-1));
	gl(BindTexture(GL_TEXTURE_2D, tex.handle));
}
inline void bindTexture2D(Texture2D tex, uint unit) {
	lftAssert(tex.handle != GLuint(-1));
	activeTexture(unit);
	gl(BindTexture(GL_TEXTURE_2D, tex.handle));
}
inline void unbindTexture2D() {
	gl(BindTexture(GL_TEXTURE_2D, 0));
}
inline void unbindTexture2D(uint unit) {
	activeTexture(unit);
	gl(BindTexture(GL_TEXTURE_2D, 0));
}
inline Texture2D boundTexture2D() {
	GLint bound;
	gl(GetIntegerv(GL_TEXTURE_BINDING_2D, &bound));
	Texture2D tex;
	tex.handle = bound > 0 ? bound : GLuint(-1);
	return tex;
}

inline void uploadTexture2D(const void* data, uint width, uint height, GLenum type, GLenum format, GLenum internal_format) {
	lftAssert(boundTexture2D().handle != GLuint(-1));
	gl(TexImage2D(GL_TEXTURE_2D, 0, internal_format, width, height, 0, format, type, data));
}

inline void setFilterTexture2D(GLenum filter_mode) {
	lftAssert(boundTexture2D().handle != GLuint(-1));
	gl(TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter_mode));
	gl(TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter_mode));
}
inline void setWrapTexture2D(GLenum wrap_mode) {
	lftAssert(boundTexture2D().handle != GLuint(-1));
	gl(TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap_mode));
	gl(TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrap_mode));
}

inline void createTextureCube(TextureCube& tex) {
	lftAssert(tex.handle == GLuint(-1));
	gl(GenTextures(1, &tex.handle));
}
inline void createTextureCubes(TextureCube* texs, uint sz) {
	#ifndef NDEBUG
	for(uint i = 0; i < sz; ++i) {
		lftAssert(texs[i].handle == GLuint(-1));
	}
	#endif
	lftAssert(sz);
	gl(GenTextures(sz, &texs[0].handle));
}

inline void deleteTextureCube(TextureCube& tex) {
	lftAssert(tex.handle != GLuint(-1));
	gl(DeleteTextures(1, &tex.handle));
	tex.handle = GLuint(-1);
}
inline void deleteTextureCubes(TextureCube* texs, uint sz) {
	#ifndef NDEBUG
	for(uint i = 0; i < sz; ++i) {
		lftAssert(texs[i].handle == GLuint(-1));
	}
	#endif
	lftAssert(sz);
	gl(GenTextures(sz, &texs[0].handle));
	for(uint i = 0; i < sz; ++i) {
		texs[i].handle = GLuint(-1);
	}
}

inline void bindTextureCube(TextureCube tex) {
	lftAssert(tex.handle != GLuint(-1));
	gl(BindTexture(GL_TEXTURE_CUBE_MAP, tex.handle));
}
inline void bindTextureCube(TextureCube tex, uint unit) {
	lftAssert(tex.handle != GLuint(-1));
	activeTexture(unit);
	gl(BindTexture(GL_TEXTURE_CUBE_MAP, tex.handle));
}
inline void unbindTextureCube() {
	gl(BindTexture(GL_TEXTURE_CUBE_MAP, 0));
}
inline void unbindTextureCube(uint unit) {
	activeTexture(unit);
	gl(BindTexture(GL_TEXTURE_CUBE_MAP, 0));
}
inline TextureCube boundTextureCube() {
	GLint bound;
	gl(GetIntegerv(GL_TEXTURE_BINDING_CUBE_MAP, &bound));
	TextureCube tex;
	tex.handle = bound > 0 ? bound : GLuint(-1);
	return tex;
}

inline void uploadTextureCube(GLenum target, const void* data, uint width, uint height, GLenum type, GLenum format, GLenum internal_format) {
	lftAssert(boundTextureCube().handle != GLuint(-1));
	gl(TexImage2D(target, 0, internal_format, width, height, 0, format, type, data));
}

inline void setFilterTextureCube(GLenum filter_mode) {
	lftAssert(boundTextureCube().handle != GLuint(-1));
	gl(TexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, filter_mode));
	gl(TexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, filter_mode));
}
inline void setWrapTextureCube(GLenum wrap_mode) {
	lftAssert(boundTextureCube().handle != GLuint(-1));
	gl(TexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, wrap_mode));
	gl(TexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, wrap_mode));
	gl(TexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, wrap_mode));
}

#endif
